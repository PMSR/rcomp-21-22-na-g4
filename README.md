RCOMP 2021-2022 Project repository template
===========================================
# 1. Team members (update this information please) #
  * 1191505 - Paula Rodrigues 
  * 1160810 - José Moreira  
  * 1180651 - Jorge Rodrigues
  * 1140985 - Estefânia Paulo
  * 1200049 - João Wolff

> Any team membership changes should be reported here:
>> * Member 1191841 (Pedro Silva) has entered the team on 22-03-2022
>> * Member 1191841 (Pedro Silva) has left the team on 30-03-2022
>> * Member 1200049 (João Wolff) has entered the team on 30-03-2022

# 2. Sprints #
  * [Sprint 1](doc/sprint1/)
  * [Sprint 2](doc/sprint2/)
  * [Sprint 3](doc/sprint3/)
  * [Sprint 4](doc/sprint4/)

