RCOMP 2021-2022 Project - Sprint 1 - Member 1140985 folder
===========================================
(This folder is to be created/edited by the team member 1140985 only)

# *Building* 2

**Objective:** Define a structure cabling plan to apply in **Building 2** (floor 0 and floor 1).

## Grounded Floor - Building 2 ( Floor 0)

**Description:** 
> The ground floor has an underfloor cable raceway connected to the external technical ditch. Access to
the underfloor cable raceway is available at points marked over the plan. The ceiling height on this floor
is 4 meters.

**Floor 0 Dimensions:** 20 x 20 meters, and total area of 400 square meters.

**Requirements:** 
> Require full wireless LAN coverage (Wi-Fi).

![B2F0_Dimensions](B2_F0.jpg)
> **Figure 1:** Building 2 information and scale - Floor 0

**Grounded Floor Schematic cabling structure cabling plan:**

![B4F1_Dimensions](B2_F0_Schematic_Plan.jpg)
> **Figure 2:** Building 2 information and scale - Floor 0
>


####Measures Per room for Building 1 floor 0:
Note: Please check .xlsx file with calculations.

| Floor   | Room         | Outlets | Total Outlets | Patch cords (1m) | Total Patch cords (1m) | meters of copper cable | meter of optical fiber cable (SR4/LR) |
|---------|--------------|---------|---------------|------------------|------------------------|------------------------|---------------------------------------|
| Floor 0 | 2.0.1        | -       | -             | -                | -                      | -                      | 3/5                                   |
| Floor 0 | 2.0.2        | 8       |               | 8                |                        | 44                     | 5,5                                   |
| Floor 0 | 2.0.2        | 8       | 18            | 8                | 18                     | 68                     | -                                     |
| Floor 0 | 2.0.2        | 2       |               | 2                |                        | 24,6                   | -                                     |
| Floor 0 | 2.0.3        | 4       | 4             | 4                | 4                      | 46,8                   | 43,7                                  |
| Floor 0 | 2.0.4        | 6       | 6             | 6                | 6                      | 51,6                   | -                                     |
| Floor 0 | 2.0.5        | 6       | 6             | 6                | 6                      | 81,6                   | -                                     |
| Floor 0 | 2.0.6        | 8       | 8             | 8                | 8                      | 134,4                  | -                                     |
| Floor 0 | Common Areas | -       | 8             | -                | -                      | -                      | 18,5                                  |
| Floor 0 | **Totals**   | **42**  | **42**        | **42**           | **42**                 | **451**                | **.7**                                |



####Inventory for Building 2 floor 0:
19'' rack format
* 1 IC
* 1 HC
* 2 CP
* 2 AC
* 42 Outlets
* ≈ 5 meters of optical fiber 100GbaseLR
* ≈ ##X## meters of optical fiber 100GbaseSR4
* ≈ ##X## meters of CAT7 copper cable;
* 4 *Patch Panels* CAT7, 2U (48 ports)
* 2 *Patch Panels* optical fiber, 1U (24 ports)




**Description:**
Floor 1 has no underfloor cable raceway. The ceiling height on this floor is 3 meters, however there’s
a removable dropped ceiling, placed 2.5 meters from the ground, covering the entire floor. The space
over the dropped ceiling is perfect to install cable raceways.

![B2F1_Dimensions](B2_F1.jpg)
> **Figure 3:** Building 2 information and scale - Floor 1
> 
> **Grounded Floor Schematic cabling structure cabling plan:**

![B2F1_Dimensions](B2_F1_Schematic_Plan.jpg)
> **Figure 2:** Building 2 information and scale - Floor 0
> 
>
###Measures Per room for Building 1 floor 1:
Note: Please check .xlsx file with calculations.

| Floor   | Room           | Outlets | Total Outlets | Patch cords (1m) | Total Patch cords (1m) | meters of copper cable | meter of optical fiber cable (SR4) |
|---------|----------------|---------|---------------|------------------|------------------------|------------------------|------------------------------------|
| Floor 1 | 2.1.1          | -       | -             | -                | -                      | -                      | 2,5                                |
| Floor 1 | 2.1.2          | 6       | 6             | 6                | 6                      | 57                     | -                                  |
| Floor 1 | 2.1.3          | 6       | 6             | 6                | 6                      | 48                     | -                                  |
| Floor 1 | 2.1.4          | 6       | 6             | 6                | 6                      | 48                     | -                                  |
| Floor 1 | 2.1.5          | 6       | 6             | 6                | 6                      | 66                     | -                                  |
| Floor 1 | 2.1.6          | 6       | 6             | 6                | 6                      | 72                     | -                                  |
| Floor 1 | 2.1.7          | 2       | 2             | 2                | 2                      | 7,5                    | -                                  |
| Floor 1 | 2.1.8          | 2       | 2             | 2                | 2                      | 7,5                    | -                                  |
| Floor 1 | 2.1.9          | 6       | 6             | 6                | 6                      | 72                     | -                                  |
| Floor 1 | 2.1.10         | 6       | 6             | 6                | 6                      | 75                     | -                                  |
| Floor 1 | 2.1.11         | 6       | 6             | 6                | 6                      | 82,8                   | -                                  |
| Floor 1 | 2.1.12         | 6       | 6             | 6                | 6                      | 81                     | -                                  |
| Floor 1 | HC to CP1.1    | -       | -             | -                | -                      | -                      | 10,5                               |
| Floor 1 | HC to CP1.2    | -       | -             | -                | -                      | -                      | 15,3                               |
| Floor 1 | HC to CP1.3    | -       | -             | -                | -                      | -                      | 12,5                               |
| Floor 1 | CP1.1 to AC1.1 | -       | -             | -                | -                      | -                      | 2,5                                |
| Floor 1 | CP1.2 to AC1.2 | -       | -             | -                | -                      | -                      | 3,5                                |
| Floor 1 | CP1.3 to AC1.3 | -       | -             | -                | -                      | -                      | 2,5                                |
| Floor 1 | **Totals**     | **58**  | **58**        | **58**           | **58**                 | **616,8**              | **49,3**                           |



####Inventory for Building 2 floor 1:
* 1 HC
* 3 CP
* 3 AC
* 58 Outlets
* ≈ ##X## meters of optical fiber 100GbaseSR4
* ≈ ##X## meters of CAT7 copper cable;
* 4 *Patch Panels* CAT7, 2U (48 ports)
* 1 *Patch Panel* optical fiber, 1U (24 ports)


**General Considerations:**
* Optical fiber 100GbaseSR4 uses 8 optical fiber channels to transfer in *full-duplex* mode, maximum size 300 meter per cable;
* Monomode fiber offers higher data rates, immunity to scattering and, in addition, allows for longer cables. For this reason, this fiber is only used for communication between buildings. Inside buildings we only used multimode fiber.
* Copper cable CAT7 uses 4 twisted pairs to transfer in both directions simultaneously *full-duplex*;
* We chose to use the CAT7 cable, since comparing with the CAT6a, the CAT7 has the same speed as the previous one, but it has a doubled frequency. That is, the signal may pass through the CAT7 cable twice as far as it will pass through the CAT6a.
* Room 2.0.3 as conference room;
* Common areas don't have access to wired network outlets, only Wi-fi;
* Consolidation Points (as CPs), also known as *Transition Points* (TP) integrate the horizontal cabling system and are appropriate for high
  outlets density areas of a floor. They are used to regenerate the signal, in order to reach outputs that may be very distant or in areas with a large number of them.
* Every AC as Access Point was spotted in ceiling
* In floor 1 all the wire between segments goes over dropped ceiling;
* In floor 0, CAT45 goes ~1m high on outlets external raceway included on desks "islands";
