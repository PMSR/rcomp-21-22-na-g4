RCOMP 2021-2022 Project - Sprint 1 - Member 1160810 folder
===========================================
(*This folder is to be created/edited by the team member 1160810 only*)

# Campus Backbone and Building 1

**Objective:** Define a cabling structure system plan for implementation in building 1 and campus backbone.

## Campus Backbone
**Backbone Description:** The structured cabling project is to embrace a private closed area with five buildings, each of these buildings has two floors. These buildings are numbered from 1 to 5. An underground technical ditch with cable raceways exists and includes cable passageways for every building, it’s ready for telecommunications cabling and others. The building 1 horizontal dimensions are approximately 30 x 20 meters, the other buildings’ horizontal dimensions are approximately 20 x 20 meters. The building 1 will house the main cross-connect.  <br> <br>
**Campus Backbone Dimensions:** 50m*110m
![BackBoneDimension](BackBone_dimensions.jpg) <br>
> **Figure 1:** Dimensions and characteristics of the campus backbone 
 
**Campus Backbone cabling structure system plan:** The solution is to have each building connected to the MC via multimode optical fiber 100GbaseSR10 assuring the possibility of an high rate data Ethernet to be implemented on all buildings. Note that MC would be connected to each IC twice assuring redundancy.
![BackBoneCableStructure](BackBone_cable_structure.jpg)
> **Figure 2:** Cabling structure system plan for the campus backbone.

####Inventory for campus backbone: 
<table>
  <tr>
    <th>Material</th>
    <th>Quantity</th> 
    <th>Explanation</th> 
  </tr>
  <tr>
    <td>MC - Main cross-connect</td>
    <td>1</td> 
    <td>Only 1 MC is needed because all connections to IC's are under 1500m</td> 
  </tr>
  <tr>
    <td>IC - Intermediate cross-connect</td>
    <td>5</td> 
    <td>5 IC's needed, only 1 per building because all the connections to HC's are under 500m</td> 
  </tr>
  <tr>
    <td>multimode optical fiber 100GbaseSR10</td>
    <td>770m</td> 
	<td>B1->B2(40m+40m)+B1->B3(90m+90m)+B1->B4(150m+150m)+B1->B5(100m+100m)</td>
  </tr>
</table>

## Building 1
**Bulding description:** This building has 2 floors and holds the datacentre (room 1.0.2), it will also house the main cross-connect for the structured cabling system. Both floors must have wireless LAN coverage (Wi-Fi).	<br><br>
**Building Dimensions:** 30m*20m <br><br>
![B1F0_Dimensions](B1F0_Dimensions.jpg)
> **Figure 3:** Dimensions and characteristics of the Building 1 floor 0 

##Floor 0 cabling structure system plan:
(*For simplifying reasons, consider all above ground cabling are matched with the proper trough and that in zones where multiple cable stack are defined with the same draw*)<br>
![B1F0_cable_structure](B1F0_cable_structure.jpg)
> **Figure 4:**  Cabling structure system plan for the Building 1 floor 0 

**Outlets per area calculations:**<br>
<table>
	<tr>
		<th>room</th> 	
		<th>widht (m)</th>	
		<th>height (m)</th>	
		<th>area (m)</th>	
		<th>Min. Num. Outlets</th>
		<th>Connects to</th>
	</tr>
 	<tr>
		<td>1.0.1</td>	
		<td>3</td>	
		<td>5</td>	
		<td>15</td>
		<td>4</td>
		<td>hc</td>
	</tr>
 	<tr>
		<td>1.0.2</td>
		<td>5</td>
		<td>5</td>
		<td>25</td>
		<td>6</td>
		<td>cp0.0</td>
	</tr>
	<tr>
		<td>1.0.3</td>
		<td>4.5</td>
		<td>5</td>
		<td>22.5</td>
		<td>6</td>
		<td>cp0.0</td>
	</tr>
 	<tr>
		<td>1.0.4</td>
		<td>8</td>
		<td>5</td>
		<td>40</td>
		<td>8</td>
		<td>cp0.0</td>
	</tr>
 	<tr>
		<td>1.0.5</td>
		<td>3</td>
		<td>5</td>
		<td>15</td>
		<td>4</td>
		<td>cp0.1</td>
	</tr>
	<tr>
		<td>1.0.6</td>
		<td>3</td>
		<td>5</td>
		<td>15</td>
		<td>4</td>
		<td>cp0.1</td>
	</tr>
 	<tr>
		<td>1.0.7</td>	
		<td>3</td>	
		<td>5</td>
		<td>15</td>
		<td>4</td>
		<td>cp0.1</td>
	</tr>
	<tr>
		<td>1.0.8</td>	
		<td>5.5</td>	
		<td>5</td>
		<td>27.5</td>
		<td>6</td>
		<td>cp0.1</td>
	</tr>
 	<tr>
		<td>1.0.9</td>	
		<td>6.5	</td>
		<td>5</td>	
		<td>32.5</td>
		<td>8</td>
		<td>cp0.2</td>
	</tr>	
	<tr>
		<td>1.0.10</td>
		<td>6.5</td>
		<td>7.5</td>
		<td>48.75</td>
		<td>10</td>
		<td>cp0.2</td>
	</tr>
	<tr>
		<td>total</td>
		<td>---</td>
		<td>---</td>
		<td>---</td>
		<td>60</td>
		<td>---</td>
	</tr>
</table>

####Inventory for Building 1 floor 0:
<table>
  <tr>
    <th>Material</th>
    <th>Quantity</th> 
    <th>Explanation</th> 
  </tr>
  <tr>
    <td>19'' rack format</td>
    <td>1</td> 
    <td>To store the MC, IC and HC</td> 
  </tr>
  <tr>
    <td>IC - Intermediate cross-connect</td>
    <td>1</td> 
    <td>1 IC per building</td> 
  </tr>
  <tr>
    <td>HC - Patch Panel optical fiber, 1U (24 ports)</td>
    <td>1</td> 
	<td>1 HC per 1000 square meters</td>
  </tr>
  <tr>
    <td>CP - Patch Panel copper, 1U (24 ports)</td>
    <td>3</td> 
	<td>For cable organization / save on cable length</td>
  </tr>
  <tr>
    <td>AC</td>
    <td>1</td> 
	<td>1 AC per 50 meters diameter circle</td>
  </tr>
  <tr>
    <td>Outlets</td>
    <td>60</td> 
	<td>Minimum 2 per 10 square meters</td>
  </tr>
  <tr>
    <td>ultimode optical fiber 100GbaseSR4*</td>
    <td>~48m (check calc.)</td> 
	<td>Calculated base on measurements with an 10% tolerance</td>
  </tr>
    <td>copper CAT7 10GbaseT**</td>
    <td>~131m(check calc.)</td> 
	<td>Calculated base on measurements with an 10% tolerance</td>
  </tr>
</table>

>\*  Used 8 optical fibers to transmit in full-duplex,maximum cable length depends on the fiber used, up to 300 meters. <br>
\** CAT6A or CAT7 cable is required to reach the cable maximum length of 100 meters. CAT6 cables can also be used up to 55 meters length. Uses 4 twisted pairs to transmit in both directions in full-duplex using echo cancellation.<br>

====================================================================================================<br>
**Fiber and copper calculations:**
<table bgcolor="#000000">
	<tr>
    	<th>cable type</th>
    	<th>room</th> 
    	<th colspan="3">calculation</th> 
    	<th>result</th> 
	</tr>
  	<tr>
		<td rowspan="36">CAT7</td>
		<td rowspan="4">1.0.10</td>
		<tr><td>1.23cm - 5m</td><td>1.23cm - 5m</td><td rowspan="12" >---</td><td rowspan="3">x1+2(x2)=4.39+2(5.69)=15.77m</td></tr>	
		<tr><td>1.08cm - x1</td><td>1.40cm - x2</td></tr>	
		<tr><td>x1=5*1.08/1.23</td><td>x2=5*1.4/ 1.23</td></tr>	
		<td rowspan="4">1.0.9</td>
		<tr><td>1.23cm - 5m</td><td>1.23cm - 5m</td><td rowspan="3">x3+2(x4)=3.94+2(3.61)=11.16m</td></tr>	
		<tr><td>0.97cm - x3</td><td>0.89cm - x4</td></tr>	
		<tr><td>x3=5*0.97/1.23</td><td>x4=5*0.89/1.23</td></tr>	 
		<td rowspan="4">1.0.8</td>
		<tr><td>1.23cm - 5m</td><td>1.23cm - 5m</td><td rowspan="3">x6+x5=4.80+1.26=6.06m</td></tr>	
		<tr><td>1.18cm - x5</td><td>0.31cm - x6</td></tr>	
		<tr><td>x5=5*1.18/1.23</td><td>x6=5*0.31/1.23</td></tr>	 
		<td rowspan="4">1.0.7<br>1.0.6<br>1.0.5</td>
		<tr><td>1.23cm - 5m</td><td>1.23cm - 5m</td><td>1.23cm - 5m</td><td rowspan="3">	3(x7)+2(x8)+x9=3(4.43)+2(2.80)+1.46=20.35m</td></tr>	
		<tr><td>1.09cm - x7</td><td>0.69cm - x8</td><td>0.36cm - x9</td></tr>	
		<tr><td>x7=5*1.09/1.23</td><td>x8=5*0.69/1.23</td><td>x9=5*0.36/1.23</td></tr>	 
		<td rowspan="4">transition</td>
		<tr><td>1.23cm - 5m</td><td rowspan="4" colspan="2">---</td><td rowspan="3">2(x10)=5.20m</td></tr>	
		<tr><td>0.64cm - x10</td></tr>	
		<tr><td>x10=5*0.64/1.23</td></tr>	
		<td rowspan="4">1.0.4</td>
		<tr><td>1.23cm - 5m</td><td>1.23cm - 5m</td><td rowspan="15" >---</td><td rowspan="3">2(x11)+x12=2(4.96)+4.02=13.94m</td></tr>	
		<tr><td>1.22cm - x11</td><td>	0.99cm - x12</td></tr>	
		<tr><td>x11=5*1.22/1.23</td><td>x12=5*0.99/1.23</td></tr>	
		<td rowspan="4">1.0.3</td>
		<tr><td>1.23cm - 5m</td><td>1.23cm - 5m</td></td><td rowspan="3">2(x13)+2(x14)=2(4.39)+2(4.91)=18.6m</td></tr>	
		<tr><td>1.08cm - x13</td><td>1.21cm - x14</td></tr>	
		<tr><td>x13=5*1.08/1.23</td><td>x14=5*1.21/1.23</td></tr>	
		<td rowspan="4">1.0.2</td>
		<tr><td>1.23cm - 5m</td><td>1.23cm - 5m</td></td><td rowspan="3">2(x15)+2(x16)=2(6.54)+2(3.94)=20.96m</td></tr>	
		<tr><td>1.61cm - x15</td><td>0.97cm - x16</td></tr>	
		<tr><td>x15=5*1.61/1.23</td><td>x16=5*1.61/1.23</td></tr>	
		<td rowspan="4">1.0.1</td>
		<tr><td>1.23cm - 5m</td><td>1.23cm - 5m</td></td><td rowspan="3">x17+x18=2.15+4.88=7.03m</td></tr>	
		<tr><td>0.53cm - x17</td><td>1.2cm - x18</td></tr>	
		<tr><td>x17=5*0.53/1.23	</td><td>x18=5*1.2/1.23</td></tr> 	
	</tr>
	<tr>
		<td>Total CAT7</td>
		<td colspan="4">1.0.10+1.0.9+1.0.8+1.0.5:1.0.6:1.0.7+transition+1.0.4+1.0.3+1.0.2+1.0.1<br>=15.77+11.16+6.06+20.35+5.20+13.94+18.6+20.96+7.03</td>
		<td>=119.07 + 10%tolerance ~131m</td>
	</tr>
	<tr>
		<td rowspan="4">Optical Fiber:</td>
		<td rowspan="4">Transitions</td>
		<tr><td>1.23cm - 5m</td><td>1.23cm - 5m</td><td>1.23cm - 5m</td><td rowspan="3">x1+x2+x3=14.31m+21.79m+7.44m=43.54m+10%tolerance ~48m</td></tr>	
		<tr><td>3.52cm - x</td><td>5.36cm - x2</td><td>1.83cm - x3</td></tr>	
		<tr><td>x1=x=5*3.52/1.23=14.31m</td><td>x2=5*5.36/1.23=21.79m</td><td>x=5*1.83/1.23=7.44m</td>
	</tr>
	
</table>


##Floor 1 cabling structure system plan:

![B1F0_cable_structure](B1F1_cable_structure.jpg)
> **Figure 5:**  Cabling structure system plan for the Building 1 floor 1
 

**Outlets per area calculations:**<br>
<table bgcolor="#000000">
    <tr>
        <td>room </td>
        <td>widht(m)</td>
        <td>height(m)</td>
        <td>area (m)</td>
        <td>outlets</td>
        <td>connects to</td>
    </tr>
    <tr>
        <td>1.1.2</td>
        <td>7</td>
        <td>6.5</td>
        <td>45.5</td>
        <td>10</td>
        <td>CP1.0</td>
    </tr>
    <tr>
        <td>1.1.3</td>
        <td>8</td>
        <td>6.5</td>
        <td>52</td>
        <td>12</td>
        <td>CP1.0</td>
    </tr>
    <tr>
        <td>1.1.4</td>
        <td>2.5</td>
        <td>5</td>
        <td>12.5</td>
        <td>4</td>
        <td>CP1.0</td>
    </tr>
    <tr>
        <td>1.1.5</td>
        <td>2.5</td>
        <td>5</td>
        <td>12.5</td>
        <td>4</td>
        <td>CP2.0</td>
    </tr>
    <tr>
        <td>1.1.6</td>
        <td>2.5</td>
        <td>5</td>
        <td>12.5</td>
        <td>4</td>
        <td>CP2.0</td>
    </tr>
    <tr>
        <td>1.1.7</td>
        <td>2.5</td>
        <td>5</td>
        <td>12.5</td>
        <td>4</td>
        <td>CP2.0</td>
    </tr>
    <tr>
        <td>1.1.8</td>
        <td>2.5</td>
        <td>5</td>
        <td>12.5</td>
        <td>4</td>
        <td>CP2.0</td>
    </tr>
    <tr>
        <td>1.1.9</td>
        <td>2.5</td>
        <td>5</td>
        <td>12.5</td>
        <td>4</td>
        <td>CP1.0</td>
    </tr>
    <tr>
        <td>1.1.10</td>
        <td>2.5</td>
        <td>5</td>
        <td>12.5</td>
        <td>4</td>
        <td>CP2.0</td>
    </tr>
    <tr>
        <td>1.1.11</td>
        <td>2.5</td>
        <td>5</td>
        <td>12.5</td>
        <td>4</td>
        <td>CP2.0</td>
    </tr>
    <tr>
        <td>1.1.12</td>
        <td>2.5</td>
        <td>5</td>
        <td>12.5</td>
        <td>4</td>
        <td>CP2.0</td>
    </tr>
    <tr>
        <td>1.1.13</td>
        <td>2.5</td>
        <td>5</td>
        <td>12.5</td>
        <td>4</td>
        <td>CP2.0</td>
    </tr>
    <tr>
        <td>1.1.14</td>
        <td>5.5</td>
        <td>7</td>
        <td>38.5</td>
        <td>8</td>
        <td>CP2.0</td>
    </tr>
    <tr>
        <td>total</td>
        <td>---</td>
        <td>---</td>
        <td>---</td>
        <td>70</td>
		<td>---</td>
    </tr>
</table>


####Inventory for Building 1 floor 1:
<table bgcolor="#000000">
  <tr>
    <th>Material</th>
    <th>Quantity</th> 
    <th>Explanation</th> 
  </tr>
  <tr>
    <td>19'' rack format</td>
    <td>1</td> 
    <td>HC</td> 
  </tr> 
  <tr>
    <td>HC - Patch Panel optical fiber, 1U (24 ports)</td>
    <td>1</td> 
	<td>1 HC per 1000 square meters</td>
  </tr>
  <tr>
    <td>CP - Patch Panel copper, 2U (48 ports)</td>
    <td>2</td> 
	<td>For cable organization / save on cable length</td>
  </tr>
  <tr>
    <td>AC</td>
    <td>1</td> 
	<td>1 AC per 50 meters diameter circle</td>
  </tr>
  <tr>
    <td>Outlets</td>
    <td>70</td> 
	<td>Minimum 2 per 10 square meters</td>
  </tr>
  <tr>
    <td>ultimode optical fiber 100GbaseSR4*</td>
    <td>~48m (check calc.)</td> 
	<td>Calculated base on measurements with an 10% tolerance</td>
  </tr>
    <td>copper CAT7 10GbaseT**</td>
    <td>~131m(check calc.)</td> 
	<td>Calculated base on measurements with an 10% tolerance</td>
  </tr>
</table>

====================================================================================================<br>
**Fiber and copper calculations:** 
<table  bgcolor="#000000">
	<tr>
    	<th>cable type</th>
    	<th>room</th> 
    	<th colspan=3>calculation</th> 
    	<th>result</th> 
	</tr>
    <tr>
        <td rowspan=12>CAT7</td>
        <td rowspan=3>1.1.14</td>
        <td>1.25cm - 5m</td>
		<td rowspan=3 colspan=2></td>
        <td rowspan=3>2(x)=2*4.56=9.12m</td>
    </tr>
    <tr>
        <td>1.41cm - x</td>
    </tr>
    <tr>
        <td>x=5*1.41/1.25=4.56m</td>
    </tr>
    <tr>
        <td>1.1.13</td>
        <td>1.25cm - 5m</td>
		<td rowspan=3 colspan=2></td>
        <td rowspan=3>x3=5*7.37/1.25=29.48m</td>
    </tr>
    <tr>
        <td>to</td>
        <td>7.37cm - x3</td> 
    </tr>
    <tr>
        <td>1.1.4</td>
        <td>x3=29.48m</td> 
    </tr>
    <tr> 
        <td rowspan=3>1.1.3</td>
        <td>1.25cm - 5m</td>
		<td rowspan=3 colspan=2></td>
        <td rowspan=3>x4=12.08m</td>
    </tr>
    <tr> 
        <td>3.02cm-x4</td> 
    </tr>
    <tr> 
        <td>x4=5*3.02/1.25=12.08m</td> 
    </tr>
    <tr>
        <td rowspan=3>1.1.2</td>
        <td>1.25cm - 5m</td>
		<td rowspan=3 colspan=2></td>
        <td rowspan=3>x5=13.25</td>
    </tr>
    <tr> 
        <td>3.32cm - x5</td> 
    </tr>
    <tr> 
        <td>x5=5*3.32/1.25=13.25m</td> 
    </tr>
    <tr>
        <td>Total CAT7</td>
        <td colspan=4>1.1.14+1.1.13to1.1.4+1.1.3+1.1.2=9.12+29.48+12.08+13.25=63.93</td>
        <td>63.93 + 10%tolerance ~71m</td>
    </tr> 
    <tr>
        <td rowspan=3>Optical Fiber:</td>
        <td rowspan=3>Transitions</td>
        <td>1.25cm - 5m</td>
        <td>1.25cm - 5m</td>
        <td>1.25cm - 5m</td>
        <td rowspan=3>x1+x2+x3=7.24m+9.56m+17.6m+10%tolerance~38m</td>
    </tr>
    <tr> 
        <td>1.81cm - x</td>
        <td>2.38cm - x2</td>
        <td>4.40cm - x3</td> 
    </tr>
    <tr> 
        <td>x1=x=5*1.81/1.25=7.24m</td>
        <td>x2=5*2.38/1.25=9.56m</td>
        <td>x=5*4.40/1.25=17.6m</td>
    </tr>
</table>


### Disclaimer:
All measures presented do not follow the rigor and accuracy necessary for a real project, as well as improvements in the distribution of outlets may occur so that the room is functional for the purpose of the project.
Regarding the location of the AC's, there is the possibility of adding, as well as changing their location.
Also, because the objective of each of the rooms is not known is possible to have the necessity to add or remove outlets.

=====================================================================================================

## Rules
* *Main cross-connect* (MC), *Distributor C* (DC) or *Campus Distributor* (CD) – this is the single, and highest level, cable distribution point. Cables starting here made up the Cabling Subsystem 3, also called campus backbone, each cable is connected to an intermediate cross-connect.
* *Intermediate cross-connect* (IC), *Distributor B* (DB) or *Building Distributor* (BD) – this is the second cable distribution level, often there is one IC per building, the cables connecting to the next level are the Cabling Subsystem 2, also called building backbone or vertical backbone, each cable is connected to a horizontal cross-connect.
	* Cables connecting an *intermediate cross-connect* (IC) to a *horizontal cross-connect* (HC) are limited to 500 meters in length.
* *Horizontal cross-connect* (HC), *Distributor A* (DA) or *Floor Distributor* (FD) – usually this is the lowest level cable distribution point, cables beyond this point are no longer a backbone, they are called Cabling System 1 or horizontal cabling system and provide cable connections to end user outlets at the work area on the floor.
* Cables connecting the MC to an IC are limited to 1500 meters in length.
* The total area covered by a horizontal connection must be less than 1000 m².
* *Consolidation Point* (CP) are suitable for high density areas (like *Work Area Subsystems (WA)*) of outlets on one floor.
* *Common work areas* require copper cable outlets, because common user equipment does not support fiber connection.
* Multimode fiber cables can generally be up to 1000 meters long.
* 2 *outlets* per 10 m², however, this is just an indicative value, as it may vary according to the use that the space has. Therefore, **Number of outlets = ((area / 10) x 2) + 2**.
  This sum of two more outlets can be used to place, for example, a camcorder that was not in the initial plan.
* There must be an outlet < 3 meters away.
* *Patch cords* up to 5 meters in length are provided to end users to connect equipment to outlets.
* The length of each cable (any type) must be less than 90 meters.
* The straight line distance between the horizontal connection and the outlet must be less than 80 meters.
* *Patch Panels* 24-port CAT7 are 1U in size
* *Patch Panels* 24-port optical fiber are 1U in size

### *Access Points*
Are used to avoid radio interference as the frequency signals are not close together. Both as floor 1 and floor 0, I only used an AC as they have a 50 meter diameter cover,
and the building has dimensions of 30m x 20m.

### *Consolidation Points*
Also known as *Transition Points* (TP) integrate the horizontal cabling system and are appropriate for high
outlets density areas of a floor. They are used to regenerate the signal, in order to reach outputs that may be very distant or in areas with a large number of them.

### *Monomode vs Multimode*
Monomode fiber offers higher data rates, immunity to scattering and, in addition, allows for longer cables. For this reason, this fiber is only used for communication between buildings. Inside buildings we only used multimode fiber.

### *Copper Cable*
We chose to use the CAT7 cable, since comparing with the CAT6a, the CAT7 has the same speed as the previous one, but it has a doubled frequency. That is, the signal may pass through the CAT7 cable twice as far as it will pass through the CAT6a.

### *Telecommunications Enclosures*
The rule of multiplying the occupied space by 4 was followed so that there is possibility of expansion in the company