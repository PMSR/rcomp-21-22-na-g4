RCOMP 2021-2022 Project - Sprint 1 - Member 1191505 folder
===========================================
(*This folder is to be created/edited by the team member 1191505 only*)

# *Building* 4

**Objectivo:** definir um plano de implantação de cabeamento estruturado para o ambiente físico do edifício 4.
    
Nas imagens seguintes poderemos visualizar que o edifício é constituído por um piso técnico com *cable raceways* (representado a azul claro) e inclui passagens de cabos, tal como está representado na legenda e escala de cada figura.

![B4F0_Dimensions](B4F0_Dimensions.jpg)
> **Figura 1:** Características e medidas do Edifício 4 - Piso 0

![B4F1_Dimensions](B4F1_Dimensions.jpg)
> **Figura 2:** Características e medidas do Edifício 4 - Piso 1

O edifício 4 é constituído por dois pisos:
* *ground floor* (piso 0), 
* piso 1

**Conversão de metros para centímetros:** 5 metros ≈ 2,6 cm

| **Piso** | **Comprimento (m)** | **Largura (m)** | **Altura (m)** | **Área (m²)** |
| ---------- | ---------- | ---------- | ---------- | ---------- |
| 0 | 20 | 20 | 4 | 400 |
| 1 | 20 | 20 | 3 | 400 |

O edifício 4 deve possuir uma cobertura completa de LAN (Wi-Fi) em ambos os pisos.

**Número de outlets = ((área / 10) x 2) + 2**

-------------------------------------------------------------------------------------------------------------------------------------------

## Floor 0 (Ground Floor)

#### Medidas e áreas das salas

| Sala | Comprimento (cm) | Largura (cm) | Altura (cm) | Comprimento (m) | Largura (m) | Altura (m) | Área (m²) | *Outlets* | Comments |
| --------- | --------- | --------- | -------- | -------- | -------- | -------- |  -------- | -------- | -------- |
| Piso 0 | --------- | --------- | --------- | 20 | 20 | 4 | 400 | 58 | --------- |
| 4.0.1 | 4 | 3 | --------- | 7,7 | 5,8 | 4 | 44,66 | 12 | --------- |
| 4.0.2 | 4,6 | 1,8 | --------- | 8,8 | 3,5 | 4 | 30,8 | 10 | --------- |
| 4.0.3 | 4,6 | 1,8 | --------- | 8,8 | 3,5 | 4 | 30,8 | 10 | --------- |
| 4.0.4 | 4,6 | 3,4 | --------- | 8,8 | 6,5 | 4 | 57,2 | 14 | --------- |
| 4.0.5 | 1,8 | 1,2 | --------- | 3,5 | 2,3 | 4 | 8,1 | 0 | Área de armazenamento que pode ser usada para conexão cruzada, não requer *outlets* |
| 4.0.6 | 1,8 | 2,2 | --------- | 3,5 | 4,2 | 4 | 14,7 | 6 | --------- |
| 4.0.7 | 1,8 | 2,1 | --------- | 3,5 | 4,0 | 4 | 14 | 6 | --------- |
| Áreas comuns | --------- | --------- | --------- | --------- | --------- | 4 | 199,74 | 0 | As áreas comuns são constituídas pela zona do *hall* de entrada e os WC, não requerem *outlets* |

1. **Área comum** ≈ área total - (área 4.0.1 + área 4.0.2 + ... + 4.0.7)
                  ≈ 400 - (44,66 + 30,8 + 30,8 + 57,2 + 8,1 + 14,7 + 14) ≈ **199,74m²**

![B4F0_Cabos](B4F0_Cabos.jpg)
> **Figura 3 - Possível estrutura de cablagem do piso 0**

![B4F0_full](B4F0_full.jpg)
> **Figura 4 - Possível estrutura de cablagem do piso 0 e circulos de áreas**

#### Inventário/Material Necessário
* 19' rack format
* 1 IC 
* 1 HC
* 3 CP 
* 1 AC
* 58 *outlet's*  
* ≈ 27 metros de fibra ótica monomodo 100GbaseLR (utiliza 8 ou 20 fibras óticas até 10.000 metros de comprimento, *full-duplex* (4 ou 10 fibras para cada direção)) 
* ≈ 44 metros de fibra ótica multimodo 100GbaseSR4 (utiliza 8 fibras óticas para transmitir em *full-duplex*, o comprimento máximo do cabo depende da fibra utilizada, até 300 metros)
* ≈ 84 metros CAT7 (cabo de cobre, usa 4 pares traçados para transmitir em ambas as direções em full-duplex usando cancelamento de eco)
* 3 *Patch Panel* de cobre, 2U (48 portas)
* 1 *Patch Panel* de fibra ótica, 1U (24 portas)

----------------------------------------------------------------------------------------------------------

## Floor 1

#### Medidas e áreas das salas

| Sala | Comprimento (cm) | Largura (cm) | Altura (cm) | Comprimento (m) | Largura (m) | Altura (m) | Área (m²) | *Outlets* | Comments |
| --------- | --------- | --------- | -------- | -------- | -------- | -------- |  -------- | -------- | -------- |
| Piso 1 | --------- | --------- | --------- | 20 | 20 | 3 (ver comment) | 400 | 76 | Existe um teto rebaixado e removível, colocado a 2,5 metros, que permite fazer a instalação de canais de cabos e pontos de acesso sem fio |
| 4.1.1 | 8,5 | 2,9 | --------- | 16,3 | 5,6 | 3 (ver comment piso 1) | 91,3 | 22 | --------- |  
| 4.1.2 | 4,3 | 2,4 | --------- | 8,3 | 4,6 | 3 (ver comment piso 1) | 38,2 | 10 | --------- | 
| 4.1.3 | 4,7 | 1,8 | --------- | 9,0 | 3,5 | 3 (ver comment piso 1) | 31,5 | 10 | --------- | 
| 4.1.4 | 4,3 | 2,1 | --------- | 8,3 | 4,0 | 3 (ver comment piso 1) | 33,2 | 10 | --------- | 
| 4.1.5 | 4,7 | 1,8 | --------- | 9,0 | 3,5 | 3 (ver comment piso 1) | 31,5 | 10 | --------- | 
| 4.1.6 | 1,8 | 1,2 | --------- | 3,5 | 2,3 | 3 (ver comment piso 1) | 8,05 | 0 | Área de armazenamento que pode ser usada para a conexão cruzada e infraestruturas de *hardware* de internet, não requer *outlets* | 
| 4.1.7 | 4,7 | 3,4 | --------- | 9,0 | 6,5 | 3 (ver comment piso 1) | 58,5 | 14 | --------- |
| Common areas | --------- | --------- | --------- | --------- | --------- | 3 | 107,75 | 0 | As áreas comuns são constituídas pela zona do *hall* de entrada e os WC's, não requerem *outlets* |

1. **Área comum** ≈ área total - (área 4.1.1 + área 4.1.2 + ... + 4.1.7)
                     ≈ 400 - (91,3 + 38,2 + 31,5 + 33,2 + 31,5 + 8,05 + 58,5) ≈ **107,75m²**

![B4F1_Cabos](B4F1_Cabos.jpg)
> **Figura 5 - Possível estrutura de cablagem do piso 1**

![B4F1_full](B4F1_full.jpg)
> **Figura 6 - Possível estrutura de cablagem do piso 1 e circulos de áreas**


#### Inventário/Material Necessário
* 19'' rack format
* 1 HC
* 3 CP
* 1 AC
* 76 *outlet's*
* ≈ 38 metros de fibra ótica multimodo 100GbaseSR4 (utiliza 8 fibras óticas para transmitir em *full-duplex*, o comprimento máximo do cabo depende da fibra utilizada, até 300 metros)
* ≈ 121 metros CAT7 (cabo de cobre, usa 4 pares traçados para transmitir em ambas as direções em *full-duplex* usando cancelamento de eco)
* 4 *Patch Panel* de cobre, 2U (48 portas)
* 1 *Patch Panel* de fibra ótica, 1U (24 portas)

====================================================================================================

### ATENÇÃO:

Apesar de serem apresentadas estas medidas, estas não seguem o rigor e a exatidão necessárias para um projeto real, bem como poderão advir melhoramentos quanto á distribuição das outlets para que a sala fique funcional para o propósito do projeto.
Quanto à localização dos AC's existe a possibilidade de se acrescentar, bem como alterar a sua localização.

=====================================================================================================

## Regras
* Intermediate cross-connect (IC), Distributor B (DB) or Building Distributor (BD) — geralmente 1 IC por edifício (ligação com o MC do *backbone* e os HC's do edifício).
* Os cabos que conectam uma conexão cruzada intermediária (IC) a uma conexão cruzada horizontal (HC) são limitadas a 500 metros de comprimento.
* Horizontal cross-connect (HC), Distributor A (DA) or Floor Distributor (FD).
* Os cabos que conectam o MC a um IC são limitados a 1.500 metros de comprimento.  
* A área total coberta por uma conexão horizontal deve ser inferior a 1000 m² (aplica-se em ambos os pisos).
* *Consolidation Point* (CP) são apropriados para áreas de alta densidade de outlets num piso.
* Áreas de trabalho comuns requerem saídas de cabos de cobre, porque os equipamentos comuns do utilizador não suportam conexão de fibra.
* Cabos de fibra multimodo geralmente pode ter até 1000 metros de comprimento.
* 2 *outlets* por cada 10 m², no entanto, é apenas um valor indicativo, pois poderá variar de acordo com a utilização que o espaço tem. Assim sendo, **Número de outlets = ((área / 10) x 2) + 2**. 
  Esta soma de mais duas outlets podem ser utilizadas para colocar, por exemplo, uma câmara de filmar que não estava no plano inicial.
* Deve haver uma tomada a < 3 metros de distância.
* *Patch cords* com até 5 metros de comprimento são fornecidos aos utilizadores finais para conectar os equipamentos ás tomadas.
* O comprimento de cada cabo (qualquer tipo) deve ser inferior a 90 metros.
* A distância em linha reta entre a conexão horizontal e a saída deve ser inferior a 80 metros.
* *Patch Panels* CAT7 de 24 portas têm tamanho de 1U

### *Access Points*
São utilizados para evitar interferência de rádio, pois os sinais de frequência não são próximos. Tanto como piso 1 como no piso 0, apenas utilizei um AC uma vez que estes têm uma cobertura de 50 metros de diâmetro,
e o edifício tem dimensões de 20m x 20m.

### *Consolidation Points*
São utilizados para regenerar o sinal, de forma a possibilitar o alcance de saídas que podem estar muito distantes ou em zonas com um grande número das mesmas. 

### *Monomode vs Multimode*
A fibra monomodo oferece taxas de dados mais altas, imunidade á dispersão e, além disso, permite cabos mais longos. Por esse motivo, esta fibra é apenas utilizada na comunicação entre os edifícios.
No interior dos edifícios apenas utilizei a fibra multimodo.

### *Copper Cable*
Optei por usar o cabo CAT7, uma vez que comparando com o CAT6a, o CAT7 possui a mesma velocidade que o anterior, mas tem uma frequência dobrada. Isto é, o sinal poderá passar pelo cabo CAT7 duas vezes
mais do que passará pelo CAT6a.

### *Telecommunications Enclosures*
A regra de multiplicar o espaço ocupado por 4 será seguida para que haja possibilidade de expansão na empresa.