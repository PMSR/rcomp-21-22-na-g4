
![BackBoneDimension](gf.png)

<table>
    <tr> 
        <th>ROOM</th>
        <th>5.0.1</th>
        <th>5.0.3</th>
        <th>5.0.4</th>
        <th>5.0.5</th>
        <th>5.0.6</th>
        <th>5.0.7</th>
        <th>5.0.8</th>
        <th>5.0.9</th>
        <th>5.0.10</th>
        <th>5.0.11</th>
        <th>5.0.12</th>
        <th>TOTAL</th>
    </tr>
    <tr>
        <th>AREA M^2</th>
        <td>72</td>
        <td>12</td>
        <td>12</td>
        <td>12</td>
        <td>12</td>
        <td>24</td>
        <td>15</td>
        <td>15</td>
        <td>15</td>
        <td>15</td>
        <td>15 </td>
    </tr>
    <tr>
    <th> OUTLETS</th>
        <td>12</td>
        <td>3</td>
        <td>2</td>
        <td>2</td>
        <td>2</td>
        <td>5</td>
        <td>4</td>
        <td>4</td>
        <td>4</td>
        <td>4</td>
        <td>4</td>
        <td>46</td>
    </tr>
    </table>

![BackBoneDimension](b1.png)

<table>
    <tr> 
        <th>ROOM</th>
        <th> 5.1.1 </th>
        <th>5.1.2 </th>
        <th>5.1.3 </th>
        <th>5.1.4 </th>
        <th>5.1.5 </th>
        <th>5.1.6 </th>
        <th>5.1.7 </th>
        <th>5.1.8 </th>
        <th>5.1.10 </th>
        <th>TOTAL </th>
    </tr>
    <tr>
        <th>AREA M^2</th> 
        <td>28 </td>
        <td>28 </td>
        <td>28 </td>
        <td>24 </td>
        <td>24 </td>
        <td>24 </td>
        <td>24 </td>
        <td>45 </td>
        <td>16 </td>
    </tr>
    <tr>
        <th> OUTLETS</th>
        <td>5 </td>
        <td>5 </td>
        <td>5 </td>
        <td>5</td>
        <td>5 </td>
        <td>5 </td>
        <td>5 </td>
        <td>10 </td>
        <td>5 </td>
        <td>50</td>
    </tr>
    </table>

<br>

####Resume

1. I started to get the requirements provided by project owner to understand the general
    usage for the cable system and each room.
2. Then I set of at scale building to determine how many outlets I need in each room
    following the next rules: network outlets should be 2 for 10m^2 , 4 outlets between
    10m^2 - 20m^2 and 6 outlets between 20m^2 - 30m^2. Outlets are pinpointed with. Outlets
    should be less than tree meters away.
    Also was installed two outlets for access point, one in each floor. Distances was
    checked and each access point cover 50m diameter per floor, that is, all the rooms will
    be WIFI coverage. They are pinpointed with.
3. After I plan the cross-connects following the information of outlets placement to
    minimize cable lengths. Standard maximum distances were checked, 90m maximum
    for CAT7 cables. Cross-connects are pinpointed with. Cable pathways was also
    checked the 90 m max. distances and are pinpointed with and.
4. Cable used is CAT7 like referred in previous point to have a good rate (10Gbps) and
    S/STP shield usage to minimize balanced signals. Optical fiber wasn’t used because
    end-user equipment isn’t usually prepared for that.
5. For the rooms 5.0.2 and 5.1.9 I will use a 19” rack vertical enclosure they are the main
    network room. In the other rooms I will use a 19” wall rack enclosure. Each enclosure
    will be equipped with UPS system to avoid electric failures, one patch panels ISO 8877
    (24 port), one switch ISO 8877 (24 port) the same as patch panels and a U unit size for
    sockets to power supply the enclosure equipment. For each enclosure I will leave 50%
    space more to other active equipment that could be added. I will provide 0.5m patch
    cords for patch panels (same quantity as ports on the patch panel) and access point,
    and 5m for each outlet to end-user.

<br>

####Inventory 

<table>
    <tr>
        <th>Equipment</th>
        <th>Quantity</th>
    </tr>
    <tr><td>Vertical 19” rack enclosure</td><td> 2 unit</td></tr>
    <tr><td>Wall 19” rack enclosure </td><td>20 unit</td></tr>
    <tr><td>Patch panel ISO 8877 24 port </td><td>22 unit</td></tr>
    <tr><td>Switch ISO 8877 24 port </td><td>22 unit</td></tr>
    <tr><td>Patch cords 0,5m </td><td>530 unit</td></tr>
    <tr><td>Patch cords 5m </td><td>94 unit</td></tr>
    <tr><td>Access point </td><td>2 unit</td></tr>
    <tr><td>UPS U </td><td>22 unit</td></tr>
    <tr><td>Socket U </td><td>22 unit</td></tr>
    <tr><td>CAT7 S/STP cable </td><td>1200m</td></tr>
    <tr><td>Labels for cable </td><td>1000</td></tr>
    <tr><td>Network outlets </td><td>96</td></tr>
</table>