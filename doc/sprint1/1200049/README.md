RCOMP 2021-2022 Project - Sprint 1 - Member 1200049 folder
===========================================
(This folder is to be created/edited by the team member 1200049 only)

# Edifício 5

## Regras gerais utilizadas em ambos andares (normas de cablagem estruturada, escalas e notas):

- Em cada 1000m^2 de area bruta tem de haver 1HC.
- Cross connects e consolidation points são colocados em pontos estrategicos para diminuir a utilização de cabos.
- Cabos de qualquer tipo não podem exceder 90m
- o que diz respeito ao tipo de cabo e à conexão dos CPS com os outlets, foi utilizado o CAT7, devido a sua melhor performance.
- Para o cálulo do alance dos access points foi utilizado um diametro de 50m.
- Na backbone dentro do edificio, foi sempre considerada a redundância (utilizando fibra ótica monomodo)
- Para o cálculo da quantidade de patch cords, foi utilizado o número de patch panel ports * 0.5m
- Para o cálculo da quantidade de patch cords para outlets, foi considerado uma patch cord de 5m.
- Para uma melhor compatibilidade futura e visto que os APs estão ligados a tomadas CAT7, todos os patch panels de cobre e todos os patch cords de cobre serão CAT7.
- Para facilidade de expansão dos enclosures os armarios de hardware devem possuir 50% mais capacidade do que o necessario inicialmente.




Medições feitas com base na escala fornecida de 3cm ~= 5m

### Dimensões

19,7 x 19,7 = 388 m^2

# Piso 0

## Medições

![E1-P0](./FLOOR0MEASURES.png)

Tendo com base a imagem acima, e o calculo de 2 outlets por cada 10m² (sem necessidade de abraanger os WC's ou a sala 5.0.2 que sera utilizada como deposito):

**Tabela de Medidas**

| Room  | Area (m2)  | Nº de outlets 
|---|---|---
|5.0.1 |	65,9	    |	14	|
|5.0.3 |	11,97		|	4	|
|5.0.4 |	11,97	    |	4	|
|5.0.5 |	10,4		|	4	|
|5.0.6 |	10,4		|	4	|
|5.0.7 |	27,5    	|	6	|
|5.0.8 |	14,7		|	4	|
|5.0.9 |	14,7		|	4	|
|5.0.10|	16,6		|	4	|
|5.0.11|	16,6		|	4	|
|5.0.12|	16,2		|	4	|
|TOTAL | ------ | 56|

## Cablagem, distribuição de cross connects e access point

![E1-c-P0](./connections_floor_00.png)

## Comprimento de cabos calculados:

| Room |Cabo Fibra Óptica Multimodo Necessário (m)| CAT7 cabo necessário (m)|
|---|---|---|
|5.0.1 |	----	    |	49	|
|5.0.2 |	2,21	    |	0,7	|
|5.0.3 |	----		|	43,7|
|5.0.4 |	----	    |	36,1|
|5.0.5 |	----		|	24,3|
|5.0.6 |	----		|	8   |
|5.0.7 |	----    	|	36  |
|5.0.8 |	----		|	51,4|
|5.0.9 |	----		|	53,4|
|5.0.10|	----		|	55,4|
|5.0.11|	----		|	57,4|
|5.0.12|	----		|	59,4|
|Under floor passageway|		37,55	|	36,55	|
|TOTAL | 39,76 | 511,35|

## Justificação:
- dados obtidos baseados na imagem abaixo:
![E1-c1-P0](./connections_measures_floor_00.png)
- Para os ambientes com repetições foi calculada o valor mais alto e feita a multiplicação.
- Para todo o andar foram utilizados 2 cross connects, um IC e um HC posicionados na sala de equipamentos disponibilizada pelo cliente (5.0.2), assim como um consolidation point para melhor distribuição dos cabos para outlets.
- Para suportar devidamente todos outlets conectados ao HC na sala 5.0.2 será utilizado um patch panel de CAT7 com 48 ISO 8877 female connectors (2U) e mais um  1U panel de fibra otica para receber a conexão inicial do IC.
- Para suportas a ligação inicial de fibra otica no IC será utlizado 1U panel de fibra otica
- Para o consolidation point também sera necessario um patch panel de CAT7 com 48 ISO 8877 female connectors (2U) e mais um  1U panel de fibra otica para receber a conexão inicial do IC. 

## Posicionamento do Access Point:

- Visto que o access point possui uma cobertura de aproximadamente 50 metros de diametro, optou-se em centralizar o aparelho para distribuição uniforme no andar do edificio 5 de tal forma:
![E1-ap1-P0](./point-diameter.jpg)


## Inventario do andar 0

| Equipmento |	Quantidade |
|---|---|
|Cabo CAT 7 (m) (incluindo patch cords)					|	511,35     |
|Patch cords CAT7 (0,5m)(1 por patch panel port)   		|	96         |
|Cabo Fibre Óptica (m)                      			|	39,76   	|
|Patch cords fibra (0,5m)(1 por patch panel port)   	|	18         |
|Access Point 											|	1				|
|Outlets												|	56				|
|Consolidation Point									|	1				|
|Horizontal Cross-Connect								|	1				|
|Intermediate Cross-Connect								|	1				|
|Main Cross-Connect										|	0				|
|Patch Panel ISO 8877 2U (48 entradas)					|	2			|
|Fiber Patch Panel 1U (6 entradas)						|	3				|
|Telecommunication Enclosure 6U						    |	2				|



# Piso 1

## Medições

![E1-P0](./FLOOR1MEASURES.png)

Tendo com base a imagem acima, e o calculo de 2 outlets por cada 10m² (sem necessidade de abraanger os WC's):

**Tabela de Medidas**

| Room  | Area (m2)  | Nº de outlets 
|---|---|---
|5.1.1 |	24	    |	6	|
|5.1.2 |	24		|	6	|
|5.1.3 |	24,7		|	6	|
|5.1.4 |	16,5	    |	4	|
|5.1.5 |	16,5		|	4	|
|5.1.6 |	17,75		|	4	|
|5.1.7 |	17,75    	|	4	|
|5.1.8 |	35,7		|	8	|
|5.1.10 |	14,4		|	4	|
|TOTAL | ------ | 46|

## Cablagem, distribuição de cross connects e access point

![E1-c-P0](./connections_floor_01.png)

## Comprimento de cabos calculados:

No andar 1 não existem tuneis subterraneos, porém possuimos um teto falso que nos possibilita guardar os cabos a 2,5m do chao.

Tal teto falso foi utilizado 14 vezes, contando subidas e descidas, ou seja 94 * 2,5 = 230m de cabos CAT7 a mais e 2 * 2,5 = 5m de cabo de fibra.

| Room |Cabo Fibra Óptica Multimodo Necessário (m)| CAT7 cabo necessário (m)|
|---|---|---|
|5.1.1 |	----	    |	79,3	|
|5.1.2 |	----	    |	55,3	|
|5.1.3 |	----		|	35,3	|
|5.1.4 |	----	    |	88   	|
|5.1.5 |	----		|	61  	|
|5.1.6 |	----		|	55  	|
|5.1.7 |	----    	|	18      |
|5.1.8 |	----		|	35,8	|
|5.1.9 |	0,5		    |	--- 	|
|5.1.10|	----		|	3,2	|
|Celling ups and downs|		5	|	230	|
|Under false celling |		9	|	33,55	|
|TOTAL | 14,5 | 694,4|

## Justificação:
- dados obtidos baseados na imagem abaixo:
![E1-c1-P0](./connections_measures_floor_01.png)
- Para os ambientes com repetições foi calculada a media e feita a multiplicação
- Para todo o andar foram utilizados 1 cross connects HC posicionado na sala de equipamentos disponibilizada pelo cliente (5.1.9), assim como um consolidation point para melhor distribuição dos cabos para outlets.
- Para suportar devidamente todos outlets conectados ao HC na sala 5.1.9 será utilizado um patch panel de CAT7 com 48 ISO 8877 female connectors (2U) e mais um  1U panel de fibra otica para receber a conexão inicial do IC.
- Para o consolidation point também sera necessario um patch panel de CAT7 com 48 ISO 8877 female connectors (2U) para permitir futuras expansões com facilidade e mais um  1U panel de fibra otica para receber a conexão inicial do IC. 

## Posicionamento do Access Point:

- Visto que o access point possui uma cobertura de aproximadamente 50 metros de diametro, optou-se em centralizar o aparelho para distribuição uniforme no andar do edificio 5 da mesma forma que no piso 0, porém não diretamente abaixo do access point ja instalado.



## Inventario do andar 1

| Equipmento |	Quantidade |
|---|---|
|Cabo CAT 7 (m) (incluindo patch cords)					|	694,4     |
|Patch cords CAT7 (0,5m)(1 por patch panel port)   		|	96         |
|Cabo Fibre Óptica (m)                      			|	14,5   	|
|Patch cords fibra (0,5m)(1 por patch panel port)   	|	12         |
|Access Point 											|	2				|
|Outlets												|	46				|
|Consolidation Point									|	1				|
|Horizontal Cross-Connect								|	1				|
|Intermediate Cross-Connect								|	0				|
|Main Cross-Connect										|	0				|
|Patch Panel ISO 8877 2U (48 entradas)					|	2			|
|Fiber Patch Panel 1U (6 entradas)						|	2				|
|Telecommunication Enclosure 6U							|	2				|

Somando ambos inventarios finaliza-se então o inventario total do edificio:

## Inventario do edificio 5

| Equipmento |	Quantidade |
|---|---|
|Cabo CAT 7 (m) (incluindo patch cords)					|	1205,75  |
|Patch cords CAT7 (0,5m)(1 por patch panel port)   		|	192         |
|Cabo Fibre Óptica (m)                      			|	54,26   	|
|Patch cords fibra (0,5m)(1 por patch panel port)   	|	30         |
|Access Point 											|	2				|
|Outlets												|	102				|
|Consolidation Point									|	2				|
|Horizontal Cross-Connect								|	2				|
|Intermediate Cross-Connect								|	1				|
|Main Cross-Connect										|	0				|
|Patch Panel ISO 8877 2U (48 entradas)					|	4			|
|Fiber Patch Panel 1U (6 entradas)						|	5				|
|Telecommunication Enclosure 6U							|	4				|



