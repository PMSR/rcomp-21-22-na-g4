RCOMP 2021-2022 Project - Sprint 1 planning
===========================================
### Sprint master: 1160810 - José Moreira ###
<!--(This file is to be created/edited by the sprint master only)-->

# 1. Sprint's backlog #
In the first sprint it's expected that a structured cabling plan will be developed.

<table>
  <tr>
    <th>Task</th>
    <th>Task description </th> 
  </tr>
  <tr>
    <td>T.1.1</td>
    <td>Development of a structured cabling project for building 1, encompassing the campus backbone.  </td> 
  </tr>
  <tr>
    <td>T.1.2</td>
    <td>Development of a structured cabling project for building 2. </td> 
  </tr>
  <tr>
    <td>T.1.3 </td>
    <td>Development of a structured cabling project for building 3.</td> 
  </tr>
  <tr>
    <td>T.1.4 </td>
    <td>Development of a structured cabling project for building 4.</td> 
  </tr>
  <tr>
    <td>T.1.5</td>
    <td>Development of a structured cabling project for building 5.</td> 
  </tr>
</table>

# 2. Technical decisions and coordination #
In this section, all technical decisions taken in the planning meeting should be mentioned. 
Most importantly, all technical decisions impacting on the subtask's implementation must be settled on this meeting and specified here.

#### Examples: ####
  * Backbone cable types to be used
  * VLAN IDs to be used
  * VTP domains
  * WiFi channels
  * IPv4 networks' addresses and routers' addresses
  * Routing protocols, AS identifiers and area identifiers
  * Application protocols outlining (further coordination may be required between members)

1. Copper cable wiring standard to be adopted (either 568A or 568B)
2. Backbone cable types, cable passageways to be used, redundant links and others.

# 3. Subtasks assignment #
* 1160810 -	Development of a structured cabling project for building 1, encompassing the campus backbone.   
* 1140985 - Development of a structured cabling project for building 2.  
* 1180651 - Development of a structured cabling project for building 3. 
* 1191505 - Development of a structured cabling project for building 4. 