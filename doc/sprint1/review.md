RCOMP 2021-2022 Project - Sprint 1 review
=========================================
### Sprint master: 1160810 ###
<!-- *(This file is to be created/edited by the sprint master only)*-->

# 1. Sprint's backlog #
<table>
  <tr>
    <th>Task</th>
    <th>Task description </th> 
  </tr>
  <tr>
    <td>T.1.1</td>
    <td>Development of a structured cabling project for building 1, encompassing the campus backbone.  </td> 
  </tr>
  <tr>
    <td>T.1.2</td>
    <td>Development of a structured cabling project for building 2. </td> 
  </tr>
  <tr>
    <td>T.1.3 </td>
    <td>Development of a structured cabling project for building 3.</td> 
  </tr>
  <tr>
    <td>T.1.4 </td>
    <td>Development of a structured cabling project for building 4.</td> 
  </tr>
  <tr>
    <td>T.1.5</td>
    <td>Development of a structured cabling project for building 5.</td> 
  </tr>
</table>
# 2. Subtasks assessment #
One by one, each team member presents his/her outcomes to the team, the team assesses 		the accomplishment of the subtask backlog.
The subtask backlog accomplishment can be assessed as one of:

  * Totally implemented with no issues
  * Totally implemented with issues
  * Partially implemented with no issues
  * Partially implemented with issues

For the last three cases, a text description of what has not been implemented and present issues must be added.
Unimplemented features and issues solving is assigned to the same member on the next sprint.

(Examples)
## 2.1.  1160810 -	Development of a structured cabling project for building 1, encompassing the campus backbone.  
### Totally implemented with issues ###
Redundancy was not respected on all the CP's
## 2.2. 1140985 - Development of a structured cabling project for building 2. 
### Totally implemented with issues ###
## 2.3. 1180651 - Development of a structured cabling project for building 3.
### Totally implemented with issues ###
Floor 1 should have a router
## 2.4. 1191505 - Development of a structured cabling project for building 4. 
### Totally implemented with issues ###
