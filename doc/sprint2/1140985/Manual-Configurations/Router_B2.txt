Router#enable
Router#configure terminal
Router(config)#interface FastEthernet1/0.1
Router(config-subif)#encapsulation dot1Q 510
Router(config-subif)#ip address 172.18.206.129 255.255.255.192
Router(config-subif)#exit
Router(config)#interface FastEthernet1/0.2
Router(config-subif)#encapsulation dot1Q 511
Router(config-subif)#ip address 172.18.204.193 255.255.255.192
Router(config-subif)#interface FastEthernet1/0.3
Router(config-subif)#encapsulation dot1Q 512
Router(config-subif)#ip address 172.18.201.1 255.255.255.128
Router(config-subif)#interface FastEthernet1/0.4
Router(config-subif)#encapsulation dot1Q 513
Router(config-subif)#ip address 172.18.207.97 255.255.255.224
Router(config-subif)#interface FastEthernet1/0.5
Router(config-subif)#encapsulation dot1Q 514
Router(config-subif)#ip address 172.18.207.129 255.255.255.224
Router(config-subif)#exit
Router(config)#
Router(config)#interface FastEthernet 1/0
Router(config-if)#no shutdown
Router(config-if)#exit
Router(config)#ip route 0.0.0.0 0.0.0.0 172.18.200.1


hostname?
