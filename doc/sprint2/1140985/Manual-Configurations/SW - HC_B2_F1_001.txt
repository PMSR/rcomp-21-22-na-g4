Switch>enable
Switch#conf terminal
Switch(config)#vtp domain rc22nag4
Switch(config)#vtp mode client
Switch(config)#interface FastEthernet0/1
Switch(config-if)#
Switch(config-if)#switchport mode trunk
Switch(config-if)#
Switch(config-if)#exit
Switch(config)#interface FastEthernet1/1
Switch(config-if)#
Switch(config-if)#switchport mode trunk
Switch(config-if)#exit
#With Redundancy
Switch(config)#interface FastEthernet2/1
Switch(config-if)#
Switch(config-if)#switchport mode trunk
Switch(config-if)#exit
Switch(config)#interface FastEthernet3/1
Switch(config-if)#
Switch(config-if)#switchport mode trunk
Switch(config-if)#exit
Switch(config)#



Switch(config)#
Switch(config)#hostname HC_B2_F1_001








enable
conf terminal
vtp domain rc22nag4
vtp mode client
interface FastEthernet0/1

switchport mode trunk

exit
interface FastEthernet1/1

switchport mode trunk
exit
interface FastEthernet2/1

switchport mode trunk
exit
interface FastEthernet3/1

switchport mode trunk
exit