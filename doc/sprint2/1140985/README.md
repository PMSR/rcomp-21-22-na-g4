RCOMP 2021-2022 Project - Sprint 2 - Member 1140985 folder
===========================================
(This folder is to be created/edited by the team member 1140985 only)

# Building 2

In this **sprint 2** our team will need to create a network simulation matching the developed structured cabling for the building 4.
The focus is on the layer two infrastructures, and the layer three fundamentals (IPv4 addressing, and static routing).
This file is reporting about building 2. I used Packet Tracer software to create virtual simulation scenario of this building 2 (B2).

1. **Schematic of building 2:**
   ![Building2](pkt-Building2.png)

## IPv4 networks
**IP Address assigned to Building 2:**

- End user outlets on the ground floor: 25 nodes
- End user outlets on floor one: 50 nodes
- Wi-Fi network: 120 nodes
- DMZ (Servers, administration workstations, and network infrastructure devices): 12 nodes
- VoIP (IP-phones): 12 nodes

All Defined VLANs information regarding Building 2 (as B2):

| VLAN Location | VLAN ID | VLAN Name     | Number Nodes | End Network    | Mask            | CIDR | Broadcast Address | 1st End Available | Last End Available |
|---------------|---------|---------------|--------------|----------------|-----------------|------|-------------------|-------------------|:------------------:|
| B2 Floor 0    | 510     | VLAN_B2_F0    | 25           | 172.18.206.128 | 255.255.255.192 | /26  | 172.18.206.191    | 172.18.206.129    | 172.18.206.190     |
| B2 Floor 1    | 511     | VLAN_B2_F1    | 50           | 172.18.204.192 | 255.255.255.192 | /26  | 172.18.204.255    | 172.18.204.193    | 172.18.204.254     |
| B2 Wi-Fi      | 512     | VLAN_B2_WIFI  | 120          | 172.18.201.0   | 255.255.255.128 | /25  | 172.18.201.127    | 172.18.201.1      | 172.18.201.126     |
| B2 DMZ        | 513     | VLAN_B2_DMZ   | 12           | 172.18.207.96  | 255.255.255.224 | /27  | 172.18.207.127    | 172.18.207.97     | 172.18.207.126     |
| B2 VoIP       | 514     | VLAN_B2_VOIP  | 12           | 172.18.207.128 | 255.255.255.224 | /27  | 172.18.207.159    | 172.18.207.129    | 127.18.207.158     |
| Backbone      | 535     | VLAN_BACKBONE | 120          | 172.18.200.0   | 255.255.255.128 | /25  | 172.18.200.127    | 172.18.200.1      | 172.18.200.126     |

**NOTE:** We attribute the 1st end IP available in each network to the Gateway IP.

|    ROUTER | IP           |
|----------:|--------------|
| ROUTER_B2 | 172.18.200.3 |


## Settings and Commands

### Network Devices
- **Router_B2** :: Router of Building 2
- **IC_B2_F0_001** :: Switch of Intermediate Cross-connect (IC) of Building 2
- **HC_B2_F0_001** :: Switch of Horizontal Cross-connect (HC) of Building 2, Floor 0
- **HC_B2_F1_001** :: Switch of Horizontal Cross-connect (HC) of Building 2, Floor 1
- **Switch_B2_F0_001** :: Switch of Consolidation Point 0.1 (CP 0.1) of Building 2, Floor 0
- **Switch_B2_F0_002** :: Switch of Consolidation Point 0.2 (CP 0.2) of Building 2, Floor 0
- **Switch_B2_F1_001** :: Switch of Consolidation Point 1.1 (CP 1.1) of Building 2, Floor 1
- **Switch_B2_F1_002** :: Switch of Consolidation Point 1.2 (CP 1.2) of Building 2, Floor 1
- **Switch_B2_F1_003** :: Switch of Consolidation Point 1.3 (CP 1.3) of Building 2, Floor 1
      

###Routing Table
 ####From Outside to Building 2
It will need these static routes to outside Router connecting to Building 2 Router and we can guarantee communication:

| Destination VLAN name | VLAN ID | command route to add                                 |
|-----------------------|:-------:|:-----------------------------------------------------|
| VLAN_B2_F0            |   510   | ip route 172.18.206.128 255.255.255.192 172.18.200.3 |
| VLAN_B2_F1            |   511   | ip route 172.18.204.192 255.255.255.192 172.18.200.2 |
| VLAN_B2_WIFI          |   512   | ip route 172.18.201.0 255.255.255.128 172.18.200.2   |
| VLAN_B2_DMZ           |   513   | ip route 172.18.207.96 255.255.255.224 172.18.200.2  |
| VLAN_B2_WIFI          |   514   | ip route 172.18.207.128 255.255.255.224 172.18.200.2 |

**About all configuration commands:**
Please, check all configuration files in folders: exports-Configs/ and Manual-Configurations/ in the repository.
And feel free to open Packet Tracer simulation files (Building and Campus) .pkt




