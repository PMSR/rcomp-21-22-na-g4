!
version 15.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Router
!
!
!
!
!
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
license udi pid CISCO2811/K9 sn FTX10172QNQ-
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 ip address 172.18.200.2 255.255.255.128
 duplex auto
 speed auto
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface Modem0/1/0
 no ip address
!
interface Modem0/1/1
 no ip address
!
interface FastEthernet1/0
 no ip address
!
interface FastEthernet1/0.1
 encapsulation dot1Q 510
 ip address 172.18.206.129 255.255.255.192
!
interface FastEthernet1/0.2
 encapsulation dot1Q 511
 ip address 172.18.204.193 255.255.255.192
!
interface FastEthernet1/0.3
 encapsulation dot1Q 512
 ip address 172.18.201.1 255.255.255.128
!
interface FastEthernet1/0.4
 encapsulation dot1Q 513
 ip address 172.18.207.97 255.255.255.224
!
interface FastEthernet1/0.5
 encapsulation dot1Q 514
 ip address 172.18.207.129 255.255.255.224
!
interface Vlan1
 no ip address
 shutdown
!
ip classless
!
ip flow-export version 9
!
!
!
!
!
!
!
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end

