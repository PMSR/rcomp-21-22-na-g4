!
version 15.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Router
!
!
!
!
ip dhcp excluded-address 172.18.206.65
ip dhcp excluded-address 172.18.204.65
ip dhcp excluded-address 172.18.202.129
ip dhcp excluded-address 172.18.207.193
ip dhcp excluded-address 172.18.207.161
!
ip dhcp pool VLAN-505
 network 172.18.203.0 255.255.255.128
 default-router 172.18.203.1
ip dhcp pool VLAN-506
 network 172.18.202.0 255.255.255.128
 default-router 172.18.202.1
ip dhcp pool VLAN-507
 network 172.18.200.128 255.255.255.128
 default-router 172.18.200.129
ip dhcp pool VLAN-508
 network 172.18.201.128 255.255.255.128
 default-router 172.18.201.129
ip dhcp pool VLAN-509
 network 172.18.205.64 255.255.255.192
 default-router 172.18.205.65
ip dhcp pool Voice-509
 network 172.18.205.64 255.255.255.192
 default-router 255.255.255.224
 option 150 ip 172.18.205.65
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
license udi pid CISCO2811/K9 sn FTX10179D2B-
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 no ip address
 duplex auto
 speed auto
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface FastEthernet1/0
 no ip address
!
interface FastEthernet1/0.1
 encapsulation dot1Q 505
 ip address 172.18.203.1 255.255.255.128
!
interface FastEthernet1/0.2
 encapsulation dot1Q 506
 ip address 172.18.202.1 255.255.255.128
!
interface FastEthernet1/0.3
 encapsulation dot1Q 509
 ip address 172.18.205.65 255.255.255.192
!
interface FastEthernet1/0.4
 encapsulation dot1Q 507
 ip address 172.18.200.129 255.255.255.128
!
interface FastEthernet1/0.5
 encapsulation dot1Q 508
 ip address 190.18.201.129 255.255.255.128
!
interface Vlan1
 no ip address
 shutdown
!
router rip
!
ip classless
!
ip flow-export version 9
!
!
!
!
!
!
!
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end

