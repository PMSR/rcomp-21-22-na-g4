RCOMP 2021-2022 Project - Sprint 2 - Member 1160810 folder
===========================================
<!-- (This folder is to be created/edited by the team member 1160810 only) -->

# Building 1

In this sprint I will be to creating a network simulation matching the developed structured cabling for the building 1 and backbone. 
The focus is on the layer two infrastructures, and the layer three fundamentals (IPv4 addressing, and static routing).

1. **Schematic of building 1:**
![Building1](Building1.jpg)
   
## IPv4 networks
**IP Address assigned to Building 1:**

- End user outlets on the ground floor: 60 nodes
- End user outlets on floor one: 80 nodes
- Wi-Fi network: 120 nodes
- DMZ (Servers, administration workstations, and network infrastructure devices): 100 nodes
- VoIP (IP-phones): 40 nodes
- BackBone: 120 nodes

The following table shows all the information necessary to perform a network simulation corresponding to structured cabling, and the respective configuration of layers two and three developed with the help of Cisco Packet Tracer.

<table>
	<tr>
	    <th> Building </th>
	    <th> VLAN ID </th>
	    <th> VLAN Name </th>
	    <th> Description (*coverage*) </th>
	    <th> Number Nodes </th>
	    <th> End Network </th>
	    <th> Mask </th>
	    <th> Broadcast Address </th>
	    <th> 1st End Available </th>
	    <th> Last End Available </th>
	</tr>
    <tr>
        <td> B1 Floor 0 </td>
        <td>505</td>
        <td> VLAN_B1_F0 </td>
        <td> For all end-user outlets at the building 1 floor 0 </td>
        <td>60</td>
        <td> 172.18.203.0 </td>
        <td>25</td>
        <td> 172.18.203.127 </td>
        <td> 172.18.203.1 </td>
        <td> 172.18.203.126 </td>
    </tr>
    <tr>
        <td> B1 Floor 1 </td>
        <td>506</td>
        <td> VLAN_B1_F1 </td>
        <td> For all end-user outlets at the building 1 floor 1 </td>
        <td>80</td>
        <td> 172.18.202.0 </td>
        <td>25</td>
        <td> 172.18.202.127 </td>
        <td> 172.18.202.1 </td>
        <td> 172.18.202.126 </td>
    </tr>
    <tr>
        <td> B1 Wi-Fi </td>
        <td>507</td>
        <td> VLAN_B1_WIFI </td>
        <td> For all access-points' outlets within the building 1 </td>
        <td>120</td>
        <td> 172.18.200.128 </td>
        <td>25</td>
        <td> 172.18.200.255 </td>
        <td> 172.18.200.129 </td>
        <td> 172.18.200.254 </td>
    </tr>
    <tr>
        <td> B1 DMZ </td>
        <td>508</td>
        <td> VLAN_B1_DMZ </td>
        <td> For servers, administration workstations, and infrastructure network devices at the building 1 </td>
        <td>100</td>
        <td> 172.18.201.128 </td>
        <td>25</td>
        <td> 172.18.201.255 </td>
        <td> 172.18.201.129 </td>
        <td> 172.18.201.254 </td>
    </tr>
    <tr>
        <td> B1 VoIP </td>
        <td>509</td>
        <td> VLAN_B1_VOIP </td>
        <td> For all IP-phones within the building 1 </td>
        <td>40</td>
        <td> 172.18.205.64 </td>
        <td>26</td>
        <td> 172.18.205.127 </td>
        <td> 172.18.205.65 </td>
        <td> 172.18.205.126 </td>
    </tr>
    <tr>
        <td> Backbone </td>
        <td>535</td>
        <td> VLAN_BACKBONE </td>
        <td> For all communication across the campus backbone </td>
        <td>120</td>
        <td> 172.18.200.0 </td>
        <td>25</td>
        <td> 172.18.200.127 </td>
        <td> 172.18.200.1 </td>
        <td> 172.18.200.126</td>
    </tr>
</table>

<table>
	<tr>	
		<th> ROUTER </th>
		<th> IP  </th>
	</tr>
	<tr>
		<td> ROUTER_B1 </td>
		<td> 172.18.200.5 </td>
	</tr>
</table>

## Settings and Commands
- HC configuration:
    - **Switch(config)#** interface FastEthernet *interface*
    - **Switch(config-if)#** switchport mode access
    - **Switch(config-if)#** exit

- Configuring the switches connected to the HC:
    - **Switch(config)#** interface FastEthernet *interface*
    - **Switch(config-if)#** switchport access vlan *vlanNumber*
    - **Switch(config-if)#** exit

- Router configuration::
    - **Router(config)#** ip route *DestinationIP* *SubnetMask* *NextHop*
    - **Router(config)#** interface FastEthernet *interface*
    - **Router(config-if)#** switchport access vlan *vlanNumber*
    - **Router(config-if)#** ip address *IpAddress(VLAN defaultGateway)* *SubnetMask*
    - **Router(config-if)#** no shutdown
    - **Router(config-if)#** exit

- The laptop is connected to the Acess Point.
