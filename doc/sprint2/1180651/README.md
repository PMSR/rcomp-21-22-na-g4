RCOMP 2021-2022 Project - Sprint 2 - Member 1180651 folder
===========================================
(This folder is to be created/edited by the team member 1180651 only)

#### This is just an example for a team member with number 1180651 ####
### Each member should create a folder similar to this, matching his/her number. ###
The owner of this folder (member number 1180651) will commit here all the outcomes (results/artifacts/products)		       of his/her work during sprint 2. This may encompass any kind of standard file types.
