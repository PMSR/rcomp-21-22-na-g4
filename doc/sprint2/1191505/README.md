RCOMP 2021-2022 Project - Sprint 2 - Member 1191505 folder
===========================================
(This folder is to be created/edited by the team member 1191505 only)

# Building 4

In this sprint I will need to create a network simulation matching the developed structured cabling for the building 4. 
The focus is on the layer two infrastructures, and the layer three fundamentals (IPv4 addressing, and static routing).

1. **Schematic of building 4:**
![Building4](Building4.jpg)

## Internet Address 
ISP Node Address: **15.203.48.178/30**

## VTP domain
VTP domain: **rc22nag4**

## IPv4 networks
**IP Address assigned to Building 4:**
- End user outlets on the ground floor: 28 nodes
- End user outlets on floor one: 55 nodes
- Wi-Fi network: 70 nodes
- DMZ (Servers, administration workstations, and network infrastructure devices): 10 nodes
- VoIP (IP-phones): 12 nodes

The following table shows all the information necessary to perform a network simulation corresponding to structured cabling, and the respective configuration of layers two and three developed with the help of Cisco Packet Tracer.

| VLAN Location | VLAN ID | VLAN Name     | Number Nodes | End Network    | Mask | Broadcast Address | 1st End Available | Last End Available |
| ------------- | ------- | ------------- | ------------ | -------------- | ---- | ----------------- | ----------------- | ------------------ |
| B4 Floor 0    | 520     | VLAN_B4_F0    | 28           | 172.18.206.64  | 26   | 172.18.206.127    | 172.18.206.65     | 172.18.206.126     |
| B4 Floor 1    | 521     | VLAN_B4_F1    | 55           | 172.18.204.64  | 26   | 172.18.204.127    | 172.18.204.65     | 172.18.204.126     |
| B4 Wi-Fi      | 522     | VLAN_B4_WIFI  | 70           | 172.18.202.128 | 25   | 172.18.202.255    | 172.18.202.129    | 172.18.202.254     |
| B4 DMZ        | 523     | VLAN_B4_DMZ   | 10           | 172.18.207.192 | 27   | 172.18.207.223    | 172.18.207.193    | 172.18.207.222     |
| B4 VoIP       | 524     | VLAN_B4_VOIP  | 12           | 172.18.207.160 | 27   | 172.18.207.191    | 172.18.207.161    | 172.18.207.190     |
| Backbone      | 535     | VLAN_BACKBONE | 120          | 172.18.200.0   | 25   | 172.18.200.127    | 172.18.200.1      | 172.18.200.126     |

> **Total nodes - building 4:** 175 nodes

| ROUTER    | IP           |
|----------:| ------------ |
| ROUTER_B4 | 172.18.200.5 |

## Subnet Mask
* Mask *25 bits*: 
  > 255.255.255.128/25
* Mask *26 bits*: 
  > 255.255.255.192/26
* Mask *27 bits*: 
  > 255.255.255.224/27

## Routing table

| Building | End Network | Next Hop |
| -------- | ----------- | -------- |
| 1        | 172.18.200.128 | 172.18.200.2 |
| 1        | 172.18.201.128 | 172.18.200.2 |
| 1        | 172.18.202.0   | 172.18.200.2 |
| 1        | 172.18.203.0   | 172.18.200.2 |
| 1        | 172.18.205.64  | 172.18.200.2 |
| | |
| 2        | 172.18.201.0   | 172.18.200.3 |
| 2        | 172.18.204.192 | 172.18.200.3 |
| 2        | 172.18.206.128 | 172.18.200.3 |
| 2        | 172.18.207.96  | 172.18.200.3 |
| 2        | 172.18.207.128 | 172.18.200.3 |
| | |
| 3        | 172.18.204.0   | 172.18.200.4 |
| 3        | 172.18.205.0   | 172.18.200.4 |
| 3        | 172.18.205.192 | 172.18.200.4 |
| 3        | 172.18.206.0   | 172.18.200.4 |
| 3        | 172.18.206.192 | 172.18.200.4 |
| | |
| 4        | 172.18.202.128 | 172.18.200.5 |
| 4        | 172.18.204.64  | 172.18.200.5 |
| 4        | 172.18.206.64  | 172.18.200.5 |
| 4        | 172.18.207.160 | 172.18.200.5 |
| 4        | 172.18.207.192 | 172.18.200.5 |
| | |
| 5        | 172.18.203.128 | 172.18.200.6 |
| 5        | 172.18.204.128 | 172.18.200.6 |
| 5        | 172.18.205.128 | 172.18.200.6 |
| 5        | 172.18.207.0   | 172.18.200.6 |
| 5        | 172.18.207.64  | 172.18.200.6 |
| | |
| ISP (Internet) | 0.0.0.0        | 15.203.48.178 |
| | |
| 0.0.0.0  | 0.0.0.0        | 172.18.200.1 |

## Settings and Commands
- **HC configuration:**
    - **Switch>** enable
    - **Switch#** config terminal
    - **Switch(config)** vtp domain rc22nag4
    - **Switch(config)#** vtp mode server/client (server mode on IC and client mode on HC and other switches)
    - **Switch(config)#** interface FastEthernet *interfaceNumber*
    - **Switch(config-if)#** switchport mode access/trunk (trunk mode between the switches and router, and access mode for the end-user outlets)
    - **Switch(config-if)#** exit
    - **Switch(config)** exit
    - **Switch#** show vtp status (for see the status for the switch)
>  
- **Configuration switches connected to the HC:**
    - **Switch>** enable
    - **Switch#** config terminal
    - **Switch(config)#** interface FastEthernet *interfaceNumber*
    - **Switch(config-if)#** switchport mode access
    - **Switch(config-if)#** switchport access vlan *interfaceNumber* or switchport voice vlan *vlanNumber*
    - **Switch(config-if)#** no switchport access vlan (for the VoIP) 
    - **Switch(config-if)#** exit
    - **Switch(config)** exit
>
- **Router configuration:**
    - **Router>** enable
    - **Router#** config terminal
    - **Router(config)#** interface FastEthernet *interfaceNumber*
    - **Router(config-if)#** encapsulation dot1Q *vlanNumber*
    - **Router(config-if)#** ip address *IpAddress(VLAN defaultGateway)* *SubnetMask*
    - **Router(config-if)#** no shutdown
    - **Router(config-if)#** exit
    - **Router(config)#** ip dhcp excluded-address *IpAddress*
    - **Router(config)#** ip dhcp pool *name*  
    - **Router(dhcp-config)#** default-router *IpAddress*
    - **Router(dhcp-config)#** network *IpAddress* *SubnetMask*
    - **Router(dhcp-config)#** exit
    - **Router(config)#** exit  
    - **Router#** show ip interface breaf (to see the interface ip's)
>
- The laptop is connected to the Acess Point.

## PDU List Tests
The following image shows a PDU's list of a successful connectivity test between floor 0 and floor 1, so we can verify that devices are accessible.

![PDU's_Static_config](PDU's_Static_config.jpg)


## ICMP Connectivity Test (Ping)
The following images shows a successful connectivity test between two computers where the ping command is made from a PC on floor 0 or 1 to another PC on floor 0 or 1, so we can verify that these two devices are accessible.

1 - Connection from the PC on floor 0 to another PC and laptop on floor 0

![Ping_Floor0_Static](Ping_Floor0_Static.jpg)

2 - Connection from the PC on floor 1 to another PC and laptop on floor 1

![Ping_Floo1_Static](Ping_Floo1_Static.jpg)

3 - Connection from the laptop on floor 0 to another laptop and server on floor 1

![Ping_Floor0_to_Floor1_Static](Ping_Floor0_to_Floor1_Static.jpg)


## Final considerations

* Despite the number of end-users devices that building 4 is equipped to support, in order to keep the simulation concise and easy to read, each room that has outlets is represented by two end-users devices for each consolidation point.
  
* In this simulation, redundancy was taken into account according to the spanning tree protocol.
  
* The number of ports in each switch are not representative of the number of ports on the physical equipment considered in the structured cabling project from sprint 1.

* As it says in the statement "The IPv4 configuration of the VoIP phones is out of scope for now and will be left for the next sprint", that's why I didn't configure the ips of the phones and therefore they don't work.

* At the time I was doing the pkt of building 4 file, the backbone had not yet been made, so it does not appear in that file.

* In the file all devices were configured with static IPv4 and manually defined, including the default gateway, except for VoIP phones.
