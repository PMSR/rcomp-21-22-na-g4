RCOMP 2021-2022 Project - Sprint 2 - Member 1200049 folder
===========================================
(This folder is to be created/edited by the team member 1200049 only)

## Building 5 - Sprint 2

![PacketTracerFull](./imgs/1200049_packettracer_b5.PNG)

* this image represents the solution of the simulation in the packet tracer software, this file can be found under the packet tracer folder with the name building5.pkt
* Every cross connect is represented by a layer two equipment (switch pt empty)
* the cable redundancy according to the sprint 1 documentation is represented by the cable between HC's using the spanning tree protocol
* The simulation permits the comunication between VLANS using the default gateway, permits also the comunication with ISP by the default route in the routing tables. Every router sends unknown requests by default to the MC which can idetify any conection in between the campus.
Caso não esteja, dá foward para o ISP através de um DSL Modem.
* Every switch is in trunk mode and comprehends all the VLANS of the campus, defined in the planning.md document
* The database name, the vlans ids and names, the ip addresses for both the campus and the ISP connection were all given and are available in the planning.md document
* For the simulation of building five there were 5 different VLANS, each vlan with an IP address regarding this specifications: 
	* End user outlets on the ground floor: 40 nodes 
	* End user outlets on floor one: 55 nodes 
	* Wi-Fi network: 60 nodes 
	* DMZ (Servers, administration workstations, and network infrastructure devices): 20 nodes 
	* VoIP (IP-phones): 25 nodes

		* Total nodes: 200 nodes
				

## IPv4 Networks of building 5 ##

| VLAN Name | Number Nodes | End Network | Mask | Broadcast Address | 1st End Available | Last End Available |
| --------- | ------------ | ----------- | ---- | ------------- | ----------------- | ------------------ |
| VLAN_BACKBONE | 120 | 172.18.200.0 | 25 | 172.18.200.127 | 172.18.200.1 | 172.18.200.126 |
| VLAN_B5_WIFI | 60 | 172.18.203.128 | 25 | 172.18.203.255 | 172.18.203.129 | 172.18.203.254 |
| VLAN_B5_F1 | 55 | 172.18.204.128 | 26 | 172.18.204.191 | 172.18.204.129 | 172.18.204.190 |
| VLAN_B5_F0 | 40 | 172.18.205.128 | 26 | 172.18.205.191 | 172.18.205.129 | 172.18.205.190 |
| VLAN_B5_VOIP | 25 | 172.18.207.0 | 26 | 172.18.207.63 | 172.18.207.1 | 172.18.207.62 |
| VLAN_B5_DMZ | 20 | 172.18.207.64 | 27 | 172.18.207.95 | 172.18.207.65 | 172.18.207.94 |
		
						

## Routing Tables ##

###  MC - Router0 (IP 172.18.200.1 and 15.203.48.177) ###

| Network |	Mask | Next Hop |
|---|---|---|
| 0.0.0.0 |0.0.0.0|15.203.48.178 	|
| 172.18.200.128 | 25 | 172.18.200.2 |
| 172.18.201.128 | 25 |  172.18.200.2 |
| 172.18.202.0 | 25 |  172.18.200.2 |
| 172.18.203.0 | 25 |  172.18.200.2 |
| 172.18.205.64 | 26 |  172.18.200.2 |
| 172.18.201.0 | 25 |172.18.200.3 |
| 172.18.204.192 | 26 |  172.18.200.3 |
| 172.18.206.128 | 26 |  172.18.200.3|
| 172.18.207.96 | 27 |  172.18.200.3 |
| 172.18.207.128 | 27 | 172.18.200.3 |
| 172.18.204.0 | 26 | 172.18.200.4 |
| 172.18.205.0 | 26 |  172.18.200.4 |
| 172.18.205.192 | 26 |  172.18.200.4 |
| 172.18.206.0 | 26 |  172.18.200.4 |
| 172.18.206.192 | 26 |  172.18.200.4 |
| 172.18.204.64 | 26 | 172.18.200.5 |
| 172.18.206.64 | 26 |  172.18.200.5 |
| 172.18.202.128 | 25 |  172.18.200.5 |
| 172.18.207.160 | 27 | 172.18.200.5 |
| 172.18.207.192 | 27 | 172.18.200.5 |
| 172.18.207.0 | 26 |  172.18.200.6 |
| 172.18.207.64 | 27 | 172.18.200.6 |
| 172.18.203.128 | 25 |  172.18.200.6 |
| 172.18.204.128 | 26 |  172.18.200.6 |
| 172.18.205.128 | 26 |  172.18.200.6 |

### Building 1 - Router1.0 (IP 172.18.200.2) ###

| Network |	Mask | Next Hop |
|---|---|---|
| 0.0.0.0/0 			|		0.0.0.0	|		172.18.200.1		|

### Building 2 - Router1.0 (IP 172.18.200.3) ###

| Network |	Mask | Next Hop |
|---|---|---|
| 0.0.0.0/0 			|		0.0.0.0	|		172.18.200.1		|


### Building 3 - Router1.0 (IP 172.18.200.4) ###

| Network |	Mask | Next Hop |
|---|---|---|
| 0.0.0.0/0 			|		0.0.0.0	|		172.18.200.1		|


### Building 4 - Router1.0 (IP 172.18.200.5) ###

| Network |	Mask | Next Hop |
|---|---|---|
| 0.0.0.0/0 			|		0.0.0.0	|		172.18.200.1		|


### Building 5 - Router1.0 (IP 172.18.200.6) ###

| Network |	Mask | Next Hop |
|---|---|---|
| 0.0.0.0/0 			|		0.0.0.0	|		172.18.200.1		|


### ISP - RouterISP (15.203.48.178/30) ###

| Network |	Mask | Next Hop |
|---|---|---|
| 172.18.200.0/25 		|	255.255.255.128	|		15.203.48.177		|

---


## Configurations ##

Each equipment configuration is present under the config files folder

* all vlans were added to the database since it is all being served by the mc
* End devices were configures manually to have static IP regarding their categories.


## Tests of connection

* As expected all ping tests work succesfully inside the building and outside the building with the others with the use of the sub-interfaces and default gateways guided by the router of each building and backbone.

![PacketTracerFull](./imgs/PINGTESTS_B5.PNG)