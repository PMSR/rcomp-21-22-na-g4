RCOMP 2021-2022 Project - Sprint 2 planning
===========================================
### Sprint master: 1191505 ###
(This file is to be created/edited by the sprint master only)

# 1. Sprint's backlog #
In this second sprint we intend to create a network simulation corresponding to the structured cabling project developed in sprint one. 
Therefore, we will focus on layer two infrastructure, and layer three fundamentals (IPv4 addressing and static routing). 
For such purpose, we will use **Cisco Packet Tracer**.

# 2. Technical decisions and coordination #

In this section we will cover all the technical decisions made at the planning meeting. Therefore, all technical decisions that have an impact on the implementation of the subtasks must be defined during the planning meeting and transcribed in this document.

* Set the IPv4 network address assigned to the **backbone** network
* Define the IPv4 node address that each router will use in the **backbone** network
* Define the address book that each group member will use.

As requested by sprint two, the address blocks assigned to each team member cannot overlap with each other, neither can they overlap with the IPv4 network address assigned to the **backbone** network.

# 3. Subtasks assignment #
(For each team member (sprint master included), the description of the assigned subtask in sprint 2)

  * **1160810** - Development of a layer two and layer three Packet Tracer Simulation for building one, encompassing the campus backbone. Integration of every member’s Packet Tracer Simulation into a single simulation.
  * **1140985** - Development of a layer two and layer three Packet Tracer Simulation for building two, encompassing the campus backbone.
  * **1180651** - Development of a layer two and layer three Packet Tracer Simulation for building three, encompassing the campus backbone.
  * **1191505** - Development of a layer two and layer three Packet Tracer Simulation for building four, encompassing the campus backbone.
  * **1200049** - Development of a layer two and layer three Packet Tracer Simulation for building five, encompassing the campus backbone.

# 4. Data provided

| **Class** | **Team number (within class)** | **VTP domain name to be used** | **VLANIDs range to be used** | **IPv4 address space to be used (Block of IPv4 addresses)** | **ISP router IPv4 node address** |
| ---------- | ---------- | ---------- | ---------- | ---------- | ---------- |
| 2NA | 4 | rc22nag4 | 505 - 535 |172.18.200.0/21 | 15.203.48.178/30 |

# 5. Nomenclature and version used

**Packet Tracer Version to be adopted - 8.1.1.0022**

The defined nomenclature consists of:
* deviceType_BuildingNr_FloorNr_Number - **Example: switch_B4_F0_001**

For the VLANs, the following template is being defined:
* VLAN_Building_Type - **Example: VLAN_B4_F0** (For *Type* ca be for example Floor1, DMZ, PHONE,...)

For a project including the five buildings, there will be a total of: **(5 x 5) + 1 = 26 VLANs**

# 6. Table with VLAN´s and designations

| Building | VLAN ID | VLAN Name | Description (*coverage*) |
| -------: | ------ | --------- | ------------------------- |
| B1 Floor 0 | 505 | VLAN_B1_F0 | For all end-user outlets at the building 1 floor 0 |
| B1 Floor 1 | 506 | VLAN_B1_F1 | For all end-user outlets at the building 1 floor 1 |
| B1 Wi-Fi | 507 | VLAN_B1_WIFI | For all access-points' outlets within the building 1 |
| B1 DMZ | 508 | VLAN_B1_DMZ | For servers, administration workstations, and infrastructure network devices at the building 1 |
| B1 VoIP | 509 | VLAN_B1_VOIP | For all IP-phones within the building 1 |
| | | | |
| B2 Floor 0 | 510 | VLAN_B2_F0 | For all end-user outlets at the building 2 floor 0 |
| B2 Floor 1 | 511 | VLAN_B2_F1 | For all end-user outlets at the building 2 floor 1 |
| B2 Wi-Fi | 512 | VLAN_B2_WIFI | For all access-points' outlets within the building 2 |
| B2 DMZ | 513 | VLAN_B2_DMZ | For servers, administration workstations, and infrastructure network devices at the building 2 |
| B2 VoIP | 514 | VLAN_B2_VOIP | For all IP-phones within the building 2 |
| | | | |
| B3 Floor 0 | 515 | VLAN_B3_F0 | For all end-user outlets at the building 3 floor 0 |
| B3 Floor 1 | 516 | VLAN_B3_F1 | For all end-user outlets at the building 3 floor 1 |
| B3 Wi-Fi | 517 | VLAN_B3_WIFI | For all access-points' outlets within the building 3 |
| B3 DMZ | 518 | VLAN_B3_DMZ | For servers, administration workstations, and infrastructure network devices at the building 3 |
| B3 VoIP | 519 | VLAN_B3_VOIP | For all IP-phones within the building 3 |
| | | | |
| B4 Floor 0 | 520 | VLAN_B4_F0 | For all end-user outlets at the building 4 floor 0 |
| B4 Floor 1 | 521 | VLAN_B4_F1 | For all end-user outlets at the building 4 floor 1 |
| B4 Wi-Fi | 522 | VLAN_B4_WIFI | For all access-points' outlets within the building 4 |
| B4 DMZ | 523 | VLAN_B4_DMZ | For servers, administration workstations, and infrastructure network devices at the building 4 |
| B4 VoIP | 524 | VLAN_B4_VOIP | For all IP-phones within the building 4 |
| | | | |
| B5 Floor 0 | 525 | VLAN_B5_F0 | For all end-user outlets at the building 5 floor 0 |
| B5 Floor 1 | 526 | VLAN_B5_F1 | For all end-user outlets at the building 5 floor 1 |
| B5 Wi-Fi | 527 | VLAN_B5_WIFI | For all access-points' outlets within the building 5 |
| B5 DMZ | 528 | VLAN_B5_DMZ | For servers, administration workstations, and infrastructure network devices at the building 5 |
| B5 VoIP | 529 | VLAN_B5_VOIP | For all IP-phones within the building 5 |
| | | | |
| Backbone | 535 | VLAN_BACKBONE | For all communication across the campus backbone |

# 7. Table with IP's of the defined VLANs

### 7.1. IPv4 Networks requirements
**Building 1 and backbone:**
* End user outlets on the ground floor: 60 nodes
* End user outlets on floor one: 80 nodes
* Wi-Fi network: 120 nodes
* DMZ (Servers, administration workstations, and network infrastructure devices): 100 nodes
* VoIP (IP-phones): 40 nodes
> **Total nodes:** 400 nodes
  
> **Backbone**: 120 nodes
  
**Building 2:**
* End user outlets on the ground floor: 25 nodes
* End user outlets on floor one: 50 nodes
* Wi-Fi network: 120 nodes
* DMZ (Servers, administration workstations, and network infrastructure devices): 12 nodes
* VoIP (IP-phones): 12 nodes 
> **Total nodes:** 219 nodes
  
**Building 3:**
* End user outlets on the ground floor: 35 nodes
* End user outlets on floor one: 45 nodes
* Wi-Fi network: 55 nodes
* DMZ (Servers, administration workstations, and network infrastructure devices): 28 nodes
* VoIP (IP-phones): 25 nodes
> **Total nodes:** 188 nodes
  
**Building 4:**
* End user outlets on the ground floor: 28 nodes
* End user outlets on floor one: 55 nodes
* Wi-Fi network: 70 nodes
* DMZ (Servers, administration workstations, and network infrastructure devices): 10 nodes
* VoIP (IP-phones): 12 nodes
> **Total nodes:** 175 nodes
  
**Building 5:**
* End user outlets on the ground floor: 40 nodes
* End user outlets on floor one: 55 nodes
* Wi-Fi network: 60 nodes
* DMZ (Servers, administration workstations, and network infrastructure devices): 20 nodes
* VoIP (IP-phones): 25 nodes
> **Total nodes:** 200 nodes

### 7.2. Network and IP's available

|                               |                     |                      |
| ----------------------------: | ------------------- | -------------------- |
| Network address               |                     | 172.18.200.0         |
| Subnet Mask                   |                     | 255.255.248.0        |
| Wildcard Mask                 |                     | 0.0.7.255 ou 0.0.8.0 |
| Network address space size    |                     | 2048                 |
| 1st valid node address        | 172.18.200.0 + 1    | 172.18.200.1         |
| Next network address          | 172.18.200.0 + 2048 | 172.18.208.0         |
| Last valid node address       | 172.18.207.255 - 1  | 172.18.207.254       |
| Broadcast Address             |                     | 172.18.207.255       |

### 7.3. List IP's by VLAN

| VLAN Name | Number Nodes | End Network | Mask | Broadcast Address | 1st End Available | Last End Available |
| --------- | ------------ | ----------- | ---- | ------------- | ----------------- | ------------------ |
| VLAN_BACKBONE | 120 | 172.18.200.0 | 25 | 172.18.200.127 | 172.18.200.1 | 172.18.200.126 |
| VLAN_B1_WIFI | 120 | 172.18.200.128 | 25 | 172.18.200.255 | 172.18.200.129 | 172.18.200.254 |
| VLAN_B2_WIFI | 120 | 172.18.201.0 | 25 | 172.18.201.127 | 172.18.201.1 | 172.18.201.126 |
| VLAN_B1_DMZ | 100 | 172.18.201.128 | 25 | 172.18.201.255 | 172.18.201.129 | 172.18.201.254 |
| VLAN_B1_F1 | 80 | 172.18.202.0 | 25 | 172.18.202.127 | 172.18.202.1 | 172.18.202.126 |
| VLAN_B4_WIFI | 70 | 172.18.202.128 | 25 | 172.18.202.255 | 172.18.202.129 | 172.18.202.254 |
| VLAN_B1_F0 | 60 | 172.18.203.0 | 25 | 172.18.203.127 | 172.18.203.1 | 172.18.203.126 |
| VLAN_B5_WIFI | 60 | 172.18.203.128 | 25 | 172.18.203.255 | 172.18.203.129 | 172.18.203.254 |
| VLAN_B3_WIFI | 55 | 172.18.204.0 | 26 | 172.18.204.63 | 172.18.204.1 | 172.18.204.62 |
| VLAN_B4_F1 | 55 | 172.18.204.64 | 26 | 172.18.204.127 | 172.18.204.65 | 172.18.204.126 |
| VLAN_B5_F1 | 55 | 172.18.204.128 | 26 | 172.18.204.191 | 172.18.204.129 | 172.18.204.190 |
| VLAN_B2_F1 | 50 | 172.18.204.192 | 26 | 172.18.204.255 | 172.18.204.193 | 172.18.204.254 |
| VLAN_B3_F1 | 45 | 172.18.205.0 | 26 | 172.18.205.63 | 172.18.205.1 | 172.18.205.62 |
| VLAN_B1_VOIP | 40 | 172.18.205.64 | 26 | 172.18.205.127 | 172.18.205.65 | 172.18.205.126 |
| VLAN_B5_F0 | 40 | 172.18.205.128 | 26 | 172.18.205.191 | 172.18.205.129 | 172.18.205.190 |
| VLAN_B3_F1 | 35 | 172.18.205.192 | 26 | 172.18.205.255 | 172.18.205.193 | 172.18.205.254 |
| VLAN_B3_DMZ | 28 | 172.18.206.0 | 26 | 172.18.206.63 | 172.18.206.1 | 172.18.206.62 |
| VLAN_B4_F0 | 28 | 172.18.206.64 | 26 | 172.18.206.127 | 172.18.206.65 | 172.18.206.126 |
| VLAN_B2_F0 | 25 | 172.18.206.128 | 26 | 172.18.206.191 | 172.18.206.129 | 172.18.206.190 |
| VLAN_B3_VOIP | 25 | 172.18.206.192 | 26 | 172.18.206.255 | 172.18.206.193 | 172.18.206.254 |
| VLAN_B5_VOIP | 25 | 172.18.207.0 | 26 | 172.18.207.63 | 172.18.207.1 | 172.18.207.62 |
| VLAN_B5_DMZ | 20 | 172.18.207.64 | 27 | 172.18.207.95 | 172.18.207.65 | 172.18.207.94 |
| VLAN_B2_DMZ | 12 | 172.18.207.96 | 27 | 172.18.207.127 | 172.18.207.97 | 172.18.207.126 |
| VLAN_B2_VOIP | 12 | 172.18.207.128 | 27 | 172.18.207.159 | 172.18.207.129 | 127.18.207.158 |
| VLAN_B4_VOIP | 12 | 172.18.207.160 | 27 | 172.18.207.191 | 172.18.207.161 | 172.18.207.190 |
| VLAN_B4_DMZ | 10 | 172.18.207.192 | 27 | 172.18.207.223 | 172.18.207.193 | 172.18.207.222 |

### 8. Routing table for all buildings
To assign the addresses to the routers of each building and *backbone/MC* it's determined that the mask 172.18.200.0/25 will be used, which results in

| ROUTER    | IP           |
|----------:| ------------ |
| ROUTER_MC | 172.18.200.1 |
| ROUTER_B1 | 172.18.200.2 |
| ROUTER_B2 | 172.18.200.3 |
| ROUTER_B3 | 172.18.200.4 |
| ROUTER_B4 | 172.18.200.5 |
| ROUTER_B5 | 172.18.200.6 |