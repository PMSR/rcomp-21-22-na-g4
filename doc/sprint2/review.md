RCOMP 2021-2022 Project - Sprint 2 review
=========================================
### Sprint master: 1191505 ###
(This file is to be created/edited by the sprint master only)

# 1. Sprint's backlog #
In this second sprint we intend to create a network simulation corresponding to the structured cabling project developed in sprint one.
Therefore, we will focus on layer two infrastructure, and layer three fundamentals (IPv4 addressing and static routing).
For such purpose, we will use **Cisco Packet Tracer**.

# 2. Subtasks assessment #
One by one, each team member presents his/her outcomes to the team, the team assesses the accomplishment of the subtask backlog.
The subtask backlog accomplishment can be assessed as one of:

  * Totally implemented with no issues
  * Totally implemented with issues
  * Partially implemented with no issues
  * Partially implemented with issues

For the last three cases, a text description of what has not been implemented and present issues must be added.
Unimplemented features and issues solving is assigned to the same member on the next sprint.

## 2.1. 1160810 - Development of a layer two and layer three Packet Tracer Simulation for building 1, floors 0 and 1, encompassing the campus backbone #

Totally implemented with issues. backbone didn't comunicate properly.

## 2.2. 1140985 - Development of a layer two and layer three Packet Tracer Simulation for building 2, floors 0 and 1 #

Totally implemented with issues.

## 2.3. 1180651 - Development of a layer two and layer three Packet Tracer Simulation for building 3 #

Partially implemented with issues.

## 2.4. 1191505 - Development of a layer two and layer three Packet Tracer Simulation for building 4 #

Totally implemented with issues.

## 2.5. 1200049- Development of a layer two and layer three Packet Tracer Simulation for building 5 #

Totally implemented with issues.
