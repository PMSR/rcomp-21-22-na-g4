Router#enable
Router#configure terminal
Router(config)#interface FastEthernet1/0.1
Router(config-subif)#encapsulation dot1Q 510
Router(config-subif)#ip address 172.18.206.129 255.255.255.192
Router(config-subif)#exit
Router(config)#interface FastEthernet1/0.2
Router(config-subif)#encapsulation dot1Q 511
Router(config-subif)#ip address 172.18.204.193 255.255.255.192
Router(config-subif)#interface FastEthernet1/0.3
Router(config-subif)#encapsulation dot1Q 512
Router(config-subif)#ip address 172.18.201.1 255.255.255.128
Router(config-subif)#interface FastEthernet1/0.4
Router(config-subif)#encapsulation dot1Q 513
Router(config-subif)#ip address 172.18.207.97 255.255.255.224
Router(config-subif)#interface FastEthernet1/0.5
Router(config-subif)#encapsulation dot1Q 514
Router(config-subif)#ip address 172.18.207.129 255.255.255.224
Router(config-subif)#exit
Router(config)#
Router(config)#interface FastEthernet 1/0
Router(config-if)#no shutdown
Router(config-if)#exit
Router(config)#ip route 0.0.0.0 0.0.0.0 172.18.200.1

Router(config)#hostname Router_B2

Router(config)#ip dhcp excluded-address 172.18.206.129
Router(config)#ip dhcp excluded-address 172.18.204.193
Router(config)#ip dhcp excluded-address 172.18.201.1

Router(config)#ip dhcp pool B2F0Pool
Router(dhcp-config)#network 172.18.206.128 255.255.255.192
Router(dhcp-config)#default-router 172.18.206.129
Router(dhcp-config)#domain-name building-2.rcomp-21-22-na-g4
Router(dhcp-config)#dns-server 172.18.207.98
Router(dhcp-config)#exit

Router_B2(config)#ip dhcp pool B2F1Pool
Router_B2(dhcp-config)#network 172.18.204.192 255.255.255.192
Router_B2(dhcp-config)#default-router 172.18.204.193
Router_B2(dhcp-config)#domain-name building-2.rcomp-21-22-na-g4
Router_B2(dhcp-config)#dns-server 172.18.207.98
Router_B2(dhcp-config)#exit

Router_B2(config)#ip dhcp pool B2WifiPool
Router_B2(dhcp-config)#network 172.18.201.0 255.255.255.128
Router_B2(dhcp-config)#default-router 172.18.201.1
Router_B2(dhcp-config)#domain-name building-2.rcomp-21-22-na-g4
Router_B2(dhcp-config)#dns-server 172.18.207.98
Router_B2(dhcp-config)#exit

Router_B2(config)#ip dhcp pool VoicePool
Router_B2(dhcp-config)#network 172.18.207.128 255.255.255.224
Router_B2(dhcp-config)#default-router 172.18.207.129
Router_B2(dhcp-config)#option 150 ip 172.18.207.129
Router_B2(dhcp-config)#exit
Router(config)#telephony-service
Router(config-telephony)#auto-reg-ephone
Router(config-telephony)#ip source-address 172.18.207.129 port 2000
Router(config-telephony)#max-ephones 12
Router(config-telephony)#max-dn 12
Router(config-telephony)#auto assign 1 to 12
Router(config)#ephone-dn 1
Router(config-ephone-dn)number 201
Router(config)#ephone-dn 2
Router(config-ephone-dn)number 202
Router(config)#ephone-dn 3
Router(config-ephone-dn)number 203
Router(config)#ephone-dn 4
Router(config-ephone-dn)number 204
Router(config)#ephone-dn 5
Router(config-ephone-dn)number 205
Router(config)#ephone-dn 6
Router(config-ephone-dn)number 206
Router(config)#ephone-dn 7
Router(config-ephone-dn)number 207
Router(config)#ephone-dn 8
Router(config-ephone-dn)number 208
Router(config)#ephone-dn 9
Router(config-ephone-dn)number 209
Router(config)#ephone-dn 10
Router(config-ephone-dn)number 210
Router(config)#ephone-dn 11
Router(config-ephone-dn)number 211
Router(config)#ephone-dn 12
Router(config-ephone-dn)number 212


Router_B2(config)#router ospf 5
Router_B2(config-router)#network 172.18.206.128 0.0.0.63 area 2
Router_B2(config-router)#network 172.18.204.192 0.0.0.63 area 2
Router_B2(config-router)#network 172.18.201.0 0.0.0.127 area 2
Router_B2(config-router)#network 172.18.207.96 0.0.0.31 area 2
Router_B2(config-router)#network 172.18.207.128 0.0.0.31 area 2
Router_B2(config-router)#network 172.18.200.0 0.0.0.127 area 0
Router_B2(config-router)#exit






