Switch>enable
Switch#configure terminal
Switch(config)#vlan 510
Switch(config-vlan)# name VLAN_B2_F0
Switch(config-vlan)#vlan 511
Switch(config-vlan)# name VLAN_B2_F1
Switch(config-vlan)#vlan 512
Switch(config-vlan)# name VLAN_B2_WIFI
Switch(config-vlan)#vlan 513
Switch(config-vlan)# name VLAN_B2_DMZ
Switch(config-vlan)#vlan 514
Switch(config-vlan)# name VLAN_B2_VOIP
Switch(config-vlan)#exit
Switch(config)#vtp domain rc22nag4
Switch(config)#vtp mode server
Switch(config)#interface FastEthernet0/1
Switch(config-if)#
Switch(config-if)#switchport mode trunk
Switch(config-if)#
Switch(config-if)#exit
Switch(config)#interface FastEthernet1/1
Switch(config-if)#
Switch(config-if)#switchport mode trunk
Switch(config-if)#
Switch(config-if)#exit
Switch(config)#interface FastEthernet2/1
Switch(config-if)#
Switch(config-if)#switchport mode trunk
Switch(config-if)#
Switch(config-if)#exit
Switch(config)#
Switch(config)#interface fa 5/1
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 513


Switch(config)#
Switch(config)#hostname IC_B2_F0_001