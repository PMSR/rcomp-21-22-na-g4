Switch>enable
Switch#configure terminal
Switch(config)#vtp domain rc22nag4
Switch(config)#vtp mode client
Switch(config)#interface FastEthernet0/1
Switch(config-if)#
Switch(config-if)#switchport mode trunk
Switch(config-if)#
Switch(config-if)#exit
Switch(config)#interface FastEthernet1/1
Switch(config-if)#
Switch(config-if)#switchport mode trunk
Switch(config-if)#
Switch(config-if)#exit
Switch(config)#interface FastEthernet5/1
Switch(config-if)#
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 510
Switch(config-if)#exit
Switch(config)#interface FastEthernet6/1
Switch(config-if)#
Switch(config-if)#switchport mode access
Switch(config-if)#switchport voice vlan 514
Switch(config-if)#no switchport access vlan
Switch(config-if)#exit



Switch(config)#
Switch(config)#hostname Switch_B2_F0_001


enable
conf terminal
vtp domain rc22nag4
vtp mode client
interface FastEthernet0/1

switchport mode trunk

exit
interface FastEthernet1/1

switchport mode trunk

exit
interface FastEthernet5/1

switchport mode access
switchport access vlan 510
exit
interface FastEthernet6/1

switchport mode access
switchport access vlan 514
exit
