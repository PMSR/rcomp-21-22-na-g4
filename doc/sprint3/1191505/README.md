RCOMP 2021-2022 Project - Sprint 3 - Member 1191505 folder
===========================================
(This folder is to be created/edited by the team member 1191505 only)

# Building 4

In this sprint, I will keep working on the same network simulation from the previous sprint (concerning the same building). From the already established layer three 
configurations, now, OSPF based dynamic routing will be used to replace the previously applied static routing.

Also, I will need to do other configurations in this sprint like:
* DHCPv4 service
* VoIP service
* Adding a second server to each DMZ network to run HTTP service
* Configuring DNS servers to establish a DNS domains tree
* Enforcing NAT (Network Address Translation)
* Establishing traffic access policies on routers (static firewall).

-------------------------------------------------------
## Topology 

This is a printscreen depicting the network topology at building 4, the project itself can be found in my own folder under the name 'bulding4.pkt', now with
the additions of the HTTP Server.

![Building4](Building4.jpg)

----------------------------------------------------------
## IP Subnet

IP subnet decisions can be consulted in the previous Sprint README.md file.

The following table shows all the information necessary to perform a network simulation corresponding to structured cabling, and the respective configuration of layers two and three developed with the help of Cisco Packet Tracer.

| VLAN Location | VLAN ID | VLAN Name     | Number Nodes | End Network    | Mask | Broadcast Address | 1st End Available | Last End Available |
| ------------- | ------- | ------------- | ------------ | -------------- | ---- | ----------------- | ----------------- | ------------------ |
| B4 Floor 0    | 520     | VLAN_B4_F0    | 28           | 172.18.206.64  | 26   | 172.18.206.127    | 172.18.206.65     | 172.18.206.126     |
| B4 Floor 1    | 521     | VLAN_B4_F1    | 55           | 172.18.204.64  | 26   | 172.18.204.127    | 172.18.204.65     | 172.18.204.126     |
| B4 Wi-Fi      | 522     | VLAN_B4_WIFI  | 70           | 172.18.202.128 | 25   | 172.18.202.255    | 172.18.202.129    | 172.18.202.254     |
| B4 DMZ        | 523     | VLAN_B4_DMZ   | 10           | 172.18.207.192 | 27   | 172.18.207.223    | 172.18.207.193    | 172.18.207.222     |
| B4 VoIP       | 524     | VLAN_B4_VOIP  | 12           | 172.18.207.160 | 27   | 172.18.207.191    | 172.18.207.161    | 172.18.207.190     |
| Backbone      | 535     | VLAN_BACKBONE | 120          | 172.18.200.0   | 25   | 172.18.200.127    | 172.18.200.1      | 172.18.200.126     |

-----------------------------------------------------------
## OSPF dynamic routing

| Building       | Area id |
| -------------- | ------- |
| Backbone       | 0       |
| Building 1     | 1       |
| Building 2     | 2       |
| Building 3     | 3       |
| **Building 4** | **4**   |
| Building 5     | 5       |

### Configurations & Commands: 
**OSPF (Open Shortest Path First):**
* Static routes were deleted from the routing table, and the OSPF was established at Building 4 router as show below, it was also established in all routers.
> - **Router_B4(config)#** router ospf 4
> - **Router_B4(config-router)#** network NETWORK-ADDRESS NETWORK-WILDCARD area AREA-NUMBER
> - **Router_B4(config-router)#** network 172.18.200.128 0.0.0.127 area 1
> - **Router_B4(config-router)#** network 172.18.201.128 0.0.0.127 area 1
> - **Router_B4(config-router)#** network 172.18.202.0 0.0.0.127 area 1
> - **Router_B4(config-router)#** network 172.18.203.0 0.0.0.127 area 1
> - **Router_B4(config-router)#** network 172.18.205.64 0.0.0.63 area 1
> - **Router_B4(config-router)#** network 172.18.201.0 0.0.0.127 area 2
> - **Router_B4(config-router)#** network 172.18.204.192 0.0.0.63 area 2
> - **Router_B4(config-router)#** network 172.18.206.128 0.0.0.63 area 2
> - **Router_B4(config-router)#** network 172.18.207.96 0.0.0.31 area 2
> - **Router_B4(config-router)#** network 172.18.207.128 0.0.0.31 area 2
> - **Router_B4(config-router)#** network 172.18.204.0 0.0.0.63 area 3
> - **Router_B4(config-router)#** network 172.18.205.0 0.0.0.63 area 3
> - **Router_B4(config-router)#** network 172.18.205.192 0.0.0.63 area 3
> - **Router_B4(config-router)#** network 172.18.206.0 0.0.0.63 area 3
> - **Router_B4(config-router)#** network 172.18.206.192 0.0.0.63 area 3
> - **Router_B4(config-router)#** network 172.18.202.128 0.0.0.127 area 4
> - **Router_B4(config-router)#** network 172.18.204.64 0.0.0.63 area 4
> - **Router_B4(config-router)#** network 172.18.206.64 0.0.0.63 area 4
> - **Router_B4(config-router)#** network 172.18.207.160 0.0.0.31 area 4
> - **Router_B4(config-router)#** network 172.18.207.192 0.0.0.31 area 4
> - **Router_B4(config-router)#** network 172.18.203.128 0.0.0.127 area 5
> - **Router_B4(config-router)#** network 172.18.204.128 0.0.0.63 area 5
> - **Router_B4(config-router)#** network 172.18.205.128 0.0.0.63 area 5
> - **Router_B4(config-router)#** network 172.18.207.0 0.0.0.63 area 5
> - **Router_B4(config-router)#** network 172.18.207.64 0.0.0.31 area 5
> - **Router_B4(config-router)#** network 15.203.48.178 0.0.0.3 area 0
  
| Building | End Network | Next Hop | Wildcard | Opposite of Wildcard |
| -------- | ----------- | -------- | -------- | -------------------- |
| 1        | 172.18.200.128 | 172.18.200.2 | 0.0.0.127 | 255.255.255.128
| 1        | 172.18.201.128 | 172.18.200.2 | 0.0.0.127 | 255.255.255.128
| 1        | 172.18.202.0   | 172.18.200.2 | 0.0.0.127 | 255.255.255.128
| 1        | 172.18.203.0   | 172.18.200.2 | 0.0.0.127 | 255.255.255.128
| 1        | 172.18.205.64  | 172.18.200.2 | 0.0.0.63  | 255.255.255.192
| | | | |
| 2        | 172.18.201.0   | 172.18.200.3 | 0.0.0.127 | 255.255.255.128
| 2        | 172.18.204.192 | 172.18.200.3 | 0.0.0.63 | 255.255.255.192
| 2        | 172.18.206.128 | 172.18.200.3 | 0.0.0.63 | 255.255.255.192
| 2        | 172.18.207.96  | 172.18.200.3 | 0.0.0.31 | 255.255.255.224
| 2        | 172.18.207.128 | 172.18.200.3 | 0.0.0.31 | 255.255.255.224
| | | | |
| 3        | 172.18.204.0   | 172.18.200.4 | 0.0.0.63 | 255.255.255.192
| 3        | 172.18.205.0   | 172.18.200.4 | 0.0.0.63 | 255.255.255.192
| 3        | 172.18.205.192 | 172.18.200.4 | 0.0.0.63 | 255.255.255.192
| 3        | 172.18.206.0   | 172.18.200.4 | 0.0.0.63 | 255.255.255.192
| 3        | 172.18.206.192 | 172.18.200.4 | 0.0.0.63 | 255.255.255.192
| | | | |
| 4        | 172.18.202.128 | 172.18.200.5 | 0.0.0.127 | 255.255.255.128
| 4        | 172.18.204.64  | 172.18.200.5 | 0.0.0.63 | 255.255.255.192
| 4        | 172.18.206.64  | 172.18.200.5 | 0.0.0.63 | 255.255.255.192
| 4        | 172.18.207.160 | 172.18.200.5 | 0.0.0.31 | 255.255.255.224
| 4        | 172.18.207.192 | 172.18.200.5 | 0.0.0.31 | 255.255.255.224
| | | | |
| 5        | 172.18.203.128 | 172.18.200.6 | 0.0.0.127 | 255.255.255.128
| 5        | 172.18.204.128 | 172.18.200.6 | 0.0.0.63 | 255.255.255.192
| 5        | 172.18.205.128 | 172.18.200.6 | 0.0.0.63 | 255.255.255.192
| 5        | 172.18.207.0   | 172.18.200.6 | 0.0.0.63 | 255.255.255.192
| 5        | 172.18.207.64  | 172.18.200.6 | 0.0.0.31 | 255.255.255.224
| | | | |
| ISP (Internet) | 0.0.0.0        | 15.203.48.178 | 0.0.0.3 | 255.255.255.252 |
| | | | |
| 0.0.0.0  | 0.0.0.0        | 172.18.200.1 | | |

-----------------------------------------------------------------

## HTTP server

In addition to the already existing server, in building 4 was added to floor 1, a new server as an HTTP server for the network. It was added to the local DMZ network, as the former, it will have a manually set IPv4 address.
On this server, the HTTP service must be enabled, and was created to haver the following aspect:

![HTTP_Page](HTTP_Page.jpg)

------------------------------------------------------------------
## DHCPv4 service

The router on building 4 provide the DHCPv4 service to all local networks except for DMZ networks, were IPv4 node address are static and manually set (routers and servers).

### Configurations & Commands:
> **Floor 0:**
> - **Router_B4(config)#** ip dhcp pool floor0
> - **Router_B4(dhcp-config)#** network 172.18.206.64 255.255.255.192
> - **Router_B4(dhcp-config)#** default-router 172.18.206.65
> - **Router_B4(dhcp-config)#** domain-name building-4.rcomp-21-22-na-g4
> - **Router_B4(dhcp-config)#** dns-server 172.18.207.194

> **Floor 1:**
> - **Router_B4(config)#** ip dhcp pool floor1
> - **Router_B4(dhcp-config)#** network 172.18.204.64 255.255.255.192
> - **Router_B4(dhcp-config)#** default-router 172.18.204.65
> - **Router_B4(dhcp-config)#** domain-name building-4.rcomp-21-22-na-g4
> - **Router_B4(dhcp-config)#** dns-server 172.18.207.194

> **WiFi:**
> - **Router_B4(config)#** ip dhcp pool wifi
> - **Router_B4(dhcp-config)#** network 172.18.202.128 255.255.255.128
> - **Router_B4(dhcp-config)#** default-router 172.18.202.129
> - **Router_B4(dhcp-config)#** domain-name building-4.rcomp-21-22-na-g4
> - **Router_B4(dhcp-config)#** dns-server 172.18.207.194

> **VoIP:**
> - **Router_B4(config)#** ip dhcp pool voip
> - **Router_B4(dhcp-config)#** network 172.18.207.160 255.255.255.224
> - **Router_B4(dhcp-config)#** default-router 172.18.207.161
> - **Router_B4(dhcp-config)#** domain-name building-4.rcomp-21-22-na-g4
> - **Router_B4(dhcp-config)#** dns-server 172.18.207.194
> - **Router_B4(dhcp-config)#** option 150 ip 172.18.207.161

For the VoIP VLAN, the DHCP server configuration must include option 150, it represents the IP address of the TFTP (Trivial File Transfer Protocol) server to be used by Cisco IP phones model 7960 to download their configuration file.

> **It also guaranteed that the various gateway address were excluded from the entire pool as show bellow:**
> - **Router_B4(config)#** ip dhcp excluded-address 172.18.206.65
> - **Router_B4(config)#** ip dhcp excluded-address 172.18.204.65
> - **Router_B4(config)#** ip dhcp excluded-address 172.18.202.129
> - **Router_B4(config)#** ip dhcp excluded-address 172.18.207.161
> - **Router_B4(config)#** ip dhcp excluded-address 172.18.207.193

----------------------------------------------------------------------
## VoIP service

Ports of switches connecting to Cisco IP phones 7960, must have the voice VLAN enabled (switchport voice vlan VLANID) and the access VLAN disabled (no switchport access vlan).
Regarding VoIP call forwarding, when an incoming call is for a phone prefix assigned to another building, then the call must be forward to the telephony services running in that building’s router.

| Building | Prefix digits   | Floor | Phones Numbers   |
| -------- | --------------- | ----- | ---------------- |
| 4        | 40xx, 40yy, ... | 0     | 4001, 4002, 4003 |
| 4        | 41xx, 41yy, ... | 1     | 4101, 4102       |

### Configurations & Commands:
> - **Switch(config)#** interface FastEthernet NUMBER/NUMBER
> - **Switch(config-if)#** switchport mode access
> - **Switch(config-if)#** switchport voice vlan 524
> - **Switch(config-if)#** no switchport access vlan
> 
> - **Router_B4(config)#** telephony-service
> - **Router_B4(config-telephony)#** no auto-reg-ephone
> - **Router_B4(config-telephony)#** ip source-address 172.18.207.161 port 2000
> - **Router_B4(config-telephony)#** max-ephones 20
> - **Router_B4(config-telephony)#** max-dn 20
> - **Router_B4(config-telephony)#** exit
> - **Router_B4(config)#** ephone-dn 1
> - **Router_B4(config-ephone-dn)#** number 4001
> - **Router_B4(config-ephone-dn)#** exit
> - **Router_B4(config)#** ephone-dn 2
> - **Router_B4(config-ephone-dn)#** number 4002
> - **Router_B4(config-ephone-dn)#** exit
> - **Router_B4(config)#** ephone-dn 3
> - **Router_B4(config-ephone-dn)#** number 4003
> - **Router_B4(config-ephone-dn)#** exit
> - **Router_B4(config)#** ephone-dn 4
> - **Router_B4(config-ephone-dn)#** number 4101
> - **Router_B4(config-ephone-dn)#** exit
> - **Router_B4(config)#** ephone-dn 5
> - **Router_B4(config-ephone-dn)#** number 4102
> - **Router_B4(config-ephone-dn)#** exit
> - **Router_B4(config)#** ephone 1
> - **Router_B4(config-ephone)#** mac-address 0006.2A36.29B1
> - **Router_B4(config-ephone)#** button 1:1
> - **Router_B4(config-ephone)#** exit
> - **Router_B4(config)#** ephone 2
> - **Router_B4(config-ephone)#** mac-address 00D0.FFC2.2828
> - **Router_B4(config-ephone)#** button 1:2
> - **Router_B4(config-ephone)#** exit
> - **Router_B4(config)#** ephone 3
> - **Router_B4(config-ephone)#** mac-address 000D.BD02.88D0
> - **Router_B4(config-ephone)#** button 1:3
> - **Router_B4(config-ephone)#** exit
> - **Router_B4(config)#** ephone 4
> - **Router_B4(config-ephone)#** mac-address 0002.162E.ED82
> - **Router_B4(config-ephone)#** button 1:4
> - **Router_B4(config-ephone)#** exit
> - **Router_B4(config)#** ephone 5
> - **Router_B4(config-ephone)#** mac-address 00D0.58E9.186E
> - **Router_B4(config-ephone)#** button 1:5
> - **Router_B4(config-ephone)#** exit
> - **Router_B4(config-ephone)#** dial-peer voice 1 voip
> - **Router_B4(config-dial-peer)#** destination-pattern 1...
> - **Router_B4(config-dial-peer)#** session target ipv4:172.18.205.65
> - **Router_B4(config-dial-peer)#** exit
> - **Router_B4(config-ephone)#** dial-peer voice 2 voip
> - **Router_B4(config-dial-peer)#** destination-pattern 2...
> - **Router_B4(config-dial-peer)#** session target ipv4:172.18.207.129
> - **Router_B4(config-dial-peer)#** exit
> - **Router_B4(config-ephone)#** dial-peer voice 3 voip
> - **Router_B4(config-dial-peer)#** destination-pattern 3...
> - **Router_B4(config-dial-peer)#** session target ipv4:172.18.206.193
> - **Router_B4(config-dial-peer)#** exit 
> - **Router_B4(config-ephone)#** dial-peer voice 5 voip
> - **Router_B4(config-dial-peer)#** destination-pattern 5...
> - **Router_B4(config-dial-peer)#** session target ipv4:172.18.207.1
> - **Router_B4(config-dial-peer)#** exit


The image bellow shows the left phone calling the right phone. As indicated by the **red line**, the left phone is making a call to **4101**. The right phone is receiving 
this call from **4002**, as indicated by **blue line**.

![Ephones](Ephones.jpg)

> **Note:** Due to connection problems between the phones in Cisco Packet Tracer, it was necessary to reconfigure the connections between switches and the router. 
Therefore, all switches and routers were changed, as well as the type of cable, instead of using fiber, copper cable is used.
> Furthermore, when the cursor is placed over the switch, only the vlans defined for floor 0, floor 1, DMZ and WiFi appear. The VoIP vlan does not appear in this list 
despite the fact that it is configured for the phones to work.

----------------------------------------------------------------------
## DNS

| DNS location                  | Name                            |
| ----------------------------- | ------------------------------- |
| Backbone - DNS root domain    | rcomp-21-22-na-g4               |
| Backbone - DNS server name    | ns.rcomp-21-22-na-g4            |
| Building 4 - Local DNS domain | building-4.rcomp-21-22-na-g4    |
| Building 4 - DNS server name  | ns.building-4.rcomp-21-22-na-g4 |

IPv4 node address of the DNS name server:
> Building 4: 172.18.207.195/27 (Mask: 255.255.255.224)

The image bellow shows the DNS configurations.

![DNS](DNS.jpg)

----------------------------------------------------------------------
## NAT (Network Address Translation)

**Port Numbers:**
* 80 - HTTP
* 443 - HTTPS
* 53 - DNS

### Configurations & Commands:
> - **Router_B4(config)#** interface FastEthernet0/0.2
> - **Router_B4(config-if)#** ip nat inside
> - **Router_B4(config-if)#** exit
> - **Router_B4(config)#** interface FastEthernet0/0.1
> - **Router_B4(config-if)#** ip nat outside
> - **Router_B4(config-if)#** exit
> - **Router_B4(config)#** no access-list 1
> - **Router_B4(config)#** access-list 1 permit 172.18.207.194
> - **Router_B4(config)#** ip nat inside source list 1 interface FastEthernet0/0.2 overload
> - **Router_B4(config)#** ip nat inside source static tcp 172.18.207.194 80 172.18.200.5 80
> - **Router_B4(config)#** ip nat inside source static tcp 172.18.207.194 443 172.18.200.5 443
> - **Router_B4(config)#** ip nat inside source static tcp 172.18.207.194 53 172.18.200.5 53
> - **Router_B4(config)#** ip nat inside source static udp 172.18.207.194 53 172.18.200.5 53
>
> - **Router_B4(config)#** interface FastEthernet0/0.3
> - **Router_B4(config-if)#** ip nat inside
> - **Router_B4(config-if)#** exit
> - **Router_B4(config)#** interface FastEthernet0/0.1
> - **Router_B4(config-if)#** ip nat outside
> - **Router_B4(config-if)#** exit
> - **Router_B4(config)#** no access-list 2
> - **Router_B4(config)#** access-list 2 permit 172.18.207.194
> - **Router_B4(config)#** ip nat inside source list 2 interface FastEthernet0/0.3 overload
> - **Router_B4(config)#** ip nat inside source static tcp 172.18.207.194 80 172.18.200.5 80
> - **Router_B4(config)#** ip nat inside source static tcp 172.18.207.194 443 172.18.200.5 443
> - **Router_B4(config)#** ip nat inside source static tcp 172.18.207.194 53 172.18.200.5 53
> - **Router_B4(config)#** ip nat inside source static udp 172.18.207.194 53 172.18.200.5 53
>
> - **Router_B4(config)#** interface FastEthernet0/0.4
> - **Router_B4(config-if)#** ip nat inside
> - **Router_B4(config-if)#** exit
> - **Router_B4(config)#** interface FastEthernet0/0.1
> - **Router_B4(config-if)#** ip nat outside
> - **Router_B4(config-if)#** exit
> - **Router_B4(config)#** no access-list 3
> - **Router_B4(config)#** access-list 3 permit 172.18.207.194
> - **Router_B4(config)#** ip nat inside source list 3 interface FastEthernet0/0.4 overload
> - **Router_B4(config)#** ip nat inside source static tcp 172.18.207.194 80 172.18.200.5 80
> - **Router_B4(config)#** ip nat inside source static tcp 172.18.207.194 443 172.18.200.5 443
> - **Router_B4(config)#** ip nat inside source static tcp 172.18.207.194 53 172.18.200.5 53
> - **Router_B4(config)#** ip nat inside source static udp 172.18.207.194 53 172.18.200.5 53
>
> - **Router_B4(config)#** interface FastEthernet0/0.5
> - **Router_B4(config-if)#** ip nat inside
> - **Router_B4(config-if)#** exit
> - **Router_B4(config)#** interface FastEthernet0/0.1
> - **Router_B4(config-if)#** ip nat outside
> - **Router_B4(config-if)#** exit
> - **Router_B4(config)#** no access-list 4
> - **Router_B4(config)#** access-list 4 permit 172.18.207.194
> - **Router_B4(config)#** ip nat inside source list 4 interface FastEthernet0/0.5 overload
> - **Router_B4(config)#** ip nat inside source static tcp 172.18.207.194 80 172.18.200.5 80
> - **Router_B4(config)#** ip nat inside source static tcp 172.18.207.194 443 172.18.200.5 443
> - **Router_B4(config)#** ip nat inside source static tcp 172.18.207.194 53 172.18.200.5 53
> - **Router_B4(config)#** ip nat inside source static udp 172.18.207.194 53 172.18.200.5 53
>
> - **Router_B4(config)#** interface FastEthernet0/0.6
> - **Router_B4(config-if)#** ip nat inside
> - **Router_B4(config-if)#** exit
> - **Router_B4(config)#** interface FastEthernet0/0.1
> - **Router_B4(config-if)#** ip nat outside
> - **Router_B4(config-if)#** exit
> - **Router_B4(config)#** no access-list 5
> - **Router_B4(config)#** access-list 5 permit 172.18.207.194
> - **Router_B4(config)#** ip nat inside source list 5 interface FastEthernet0/0.6 overload
> - **Router_B4(config)#** ip nat inside source static tcp 172.18.207.194 80 172.18.200.5 80
> - **Router_B4(config)#** ip nat inside source static tcp 172.18.207.194 443 172.18.200.5 443
> - **Router_B4(config)#** ip nat inside source static tcp 172.18.207.194 53 172.18.200.5 53
> - **Router_B4(config)#** ip nat inside source static udp 172.18.207.194 53 172.18.200.5 53
> 
> - **Router_B4#** show ip nat translations

The image bellow show the NAT table.

![NAT_List](NAT_List.jpg)

----------------------------------------------------------------------  
## Static Firewall (ACLs)

ACL lists:
1. **ACL 100:** responsible for avoiding internal spoofing and allowing DNS access by the ground floor vlan.
2. **ACL 101:** responsible for avoiding internal spoofing and allowing DNS access by the first floor vlan.
3. **ACL 102:** responsible for avoiding internal spoofing and allowing DNS access by the WiFi vlan.
4. **ACL 103:** responsible for only allowing calls to leave the router through the TFTP, ITS service and the door 2000 and avoid internal spoofing. Applied at VOIP vlan.
5. **ACL 104:** responsible for avoiding external spoofing and only allowing ICMP requests to the end nodes of the building and TFTP and ITS services, OSPF traffic and traffic related to NAT, which means tcp and udp protocol directed to backbone IPv4. Everything else directed the backbone is denied. Applied in the backbone sub-interface.
6. **ACL 105:** responsible for avoiding internal spoofing and only allowing ICMP requests and tcp and udp protocols through the doors 80, 443 e 53 related to DNS and HTTP/HTTPS.

### Configurations & Commands:
> Floor 0:
> - **Router_B4(config)#** access-list 100 permit ip host 0.0.0.0 host 255.255.255.255
> - **Router_B4(config)#** access-list 100 permit ip 172.18.206.64 0.0.0.63 any
> - **Router_B4(config)#** access-list 101 permit ip host 0.0.0.0 host 255.255.255.255
> - **Router_B4(config)#** access-list 101 permit ip 172.18.204.64 0.0.0.63 any
> - **Router_B4(config)#** access-list 102 permit ip host 0.0.0.0 host 255.255.255.255
> - **Router_B4(config)#** access-list 102 permit ip 172.18.202.128 0.0.0.127 any
> - **Router_B4(config)#** access-list 103 permit ip host 0.0.0.0 host 255.255.255.255
> - **Router_B4(config)#** access-list 103 permit udp 172.18.207.160 0.0.0.31 host 172.18.200.5 eq tftp
> - **Router_B4(config)#** access-list 103 permit tcp 172.18.207.160 0.0.0.31 host 172.18.200.5 eq 2000
> - **Router_B4(config)#** access-list 103 permit ip 172.18.207.160 0.0.0.31 any
> - **Router_B4(config)#** access-list 104 deny ip 172.18.207.192 0.0.0.31 any
> - **Router_B4(config)#** access-list 104 permit ospf any host 172.18.200.5
> - **Router_B4(config)#** access-list 104 permit udp any host 172.18.200.5 eq domain
> - **Router_B4(config)#** access-list 104 permit tcp any host 172.18.200.5 eq www
> - **Router_B4(config)#** access-list 104 permit tcp any host 172.18.200.5 eq 443
> - **Router_B4(config)#** access-list 104 permit tcp any host 172.18.200.5 eq 2000
> - **Router_B4(config)#** access-list 104 deny ip any host 172.18.200.5
> - **Router_B4(config)#** access-list 104 permit ip any any
> - **Router_B4(config)#** access-list 105 deny ip 172.18.207.192 0.0.0.31 any
> - **Router_B4(config)#** access-list 105 permit udp any host 172.18.207.194 eq domain
> - **Router_B4(config)#** access-list 105 permit tcp any host 172.18.207.194 eq www
> - **Router_B4(config)#** access-list 105 permit tcp any host 172.18.207.194 eq 443

----------------------------------------------------------------------  
## PDU List Tests

The following image shows a PDU's list of examples the successful connectivity test between floor 0 and floor 1, so we can verify that devices are accessible.

![PDU_List](PDU_List.jpg)

----------------------------------------------------------------------  
## ICMP Tests

The following images shows a successful connectivity test between two end-user devices where the ping command is made from a end-user device on floor 0 or 1 to another end-user device on floor 0 or 1, so we can verify that these two devices are accessible.

1 - Connection from the PC on floor 0 to another PC and laptop on floor 1

![Test1](Test1.jpg)

2 - Connection from the PC on floor 0 to another PC and laptop on floor 0

![Test2](Test2.jpg)

3 - Connection from the PC on floor 1 to another PC and server on floor 1

![Test3](Test3.jpg)

> **Notes:** Other tests with the remaining buildings should be done. However, as they are not yet worked, I was not able to do these tests.