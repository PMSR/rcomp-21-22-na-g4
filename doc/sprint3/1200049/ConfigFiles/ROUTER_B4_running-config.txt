!
version 15.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Router
!
!
!
!
!
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
license udi pid CISCO2811/K9 sn FTX1017H757-
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 no ip address
 duplex auto
 speed auto
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface FastEthernet1/0
 no ip address
!
interface FastEthernet1/0.520
 encapsulation dot1Q 520
 ip address 172.18.206.65 255.255.255.192
!
interface FastEthernet1/0.521
 encapsulation dot1Q 521
 ip address 172.18.204.65 255.255.255.192
!
interface FastEthernet1/0.522
 encapsulation dot1Q 522
 ip address 172.18.202.129 255.255.255.128
!
interface FastEthernet1/0.523
 encapsulation dot1Q 523
 ip address 172.18.207.193 255.255.255.224
!
interface FastEthernet1/0.524
 encapsulation dot1Q 524
 ip address 172.18.207.161 255.255.255.224
!
interface FastEthernet1/0.535
 encapsulation dot1Q 535
 ip address 172.18.200.4 255.255.255.128
!
interface Vlan1
 no ip address
 shutdown
!
router rip
!
ip classless
ip route 0.0.0.0 0.0.0.0 172.18.200.1 
!
ip flow-export version 9
!
!
!
!
!
!
!
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end

