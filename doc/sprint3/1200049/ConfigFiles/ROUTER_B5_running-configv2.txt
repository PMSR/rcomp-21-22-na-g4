!
version 15.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Router
!
!
!
ip dhcp excluded-address 172.18.205.129
ip dhcp excluded-address 172.18.203.129
ip dhcp excluded-address 172.18.204.129
ip dhcp excluded-address 172.18.207.65
ip dhcp excluded-address 172.18.207.1
!
!
ip dhcp pool floor0
 network 172.18.205.128 255.255.255.192
 default-router 172.18.205.129
 dns-server 172.18.207.66
 domain-name building-5.rcomp-21-22-na-g4
ip dhcp pool floor1
 network 172.18.204.128 255.255.255.192
 default-router 172.18.204.129
 dns-server 172.18.207.66
 domain-name building-5.rcomp-21-22-na-g4
ip dhcp pool wifi
 network 172.18.203.128 255.255.255.128
 default-router 172.18.203.129
 dns-server 172.18.207.66
 domain-name building-5.rcomp-21-22-na-g4
ip dhcp pool voip
 network 172.18.207.0 255.255.255.192
 default-router 172.18.207.1
 option 150 ip 172.18.207.1
 dns-server 172.18.207.66
 domain-name building-5.rcomp-21-22-na-g4
!
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
license udi pid CISCO2811/K9 sn FTX1017H757-
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 no ip address
 duplex auto
 speed auto
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface FastEthernet1/0
 no ip address
!
interface FastEthernet1/0.525
 encapsulation dot1Q 525
 ip address 172.18.205.129 255.255.255.192
!
interface FastEthernet1/0.526
 encapsulation dot1Q 526
 ip address 172.18.204.129 255.255.255.192
!
interface FastEthernet1/0.527
 encapsulation dot1Q 527
 ip address 172.18.203.129 255.255.255.128
!
interface FastEthernet1/0.528
 encapsulation dot1Q 528
 ip address 172.18.207.65 255.255.255.224
!
interface FastEthernet1/0.529
 encapsulation dot1Q 529
 ip address 172.18.207.1 255.255.255.192
 no shutdown
!
interface FastEthernet1/0.535
 encapsulation dot1Q 535
 ip address 172.18.200.5 255.255.255.128
!
interface Vlan1
 no ip address
 shutdown
!
router ospf 5
 log-adjacency-changes
 network 172.18.200.128 0.0.0.127 area 1
 network 172.18.201.128 0.0.0.127 area 1
 network 172.18.202.0 0.0.0.127 area 1
 network 172.18.203.0 0.0.0.127 area 1
 network 172.18.205.64 0.0.0.63 area 1
 network 172.18.201.0 0.0.0.127 area 2
 network 172.18.204.192 0.0.0.63 area 2
 network 172.18.206.128 0.0.0.63 area 2
 network 172.18.207.96 0.0.0.31 area 2
 network 172.18.207.128 0.0.0.31 area 2
 network 172.18.204.0 0.0.0.63 area 3
 network 172.18.205.0 0.0.0.63 area 3
 network 172.18.205.192 0.0.0.63 area 3
 network 172.18.206.0 0.0.0.63 area 3
 network 172.18.206.192 0.0.0.63 area 3
 network 172.18.202.128 0.0.0.127 area 4
 network 172.18.204.64 0.0.0.63 area 4
 network 172.18.206.64 0.0.0.63 area 4
 network 172.18.207.160 0.0.0.31 area 4
 network 172.18.207.192 0.0.0.31 area 4
 network 172.18.203.128 0.0.0.127 area 5
 network 172.18.204.128 0.0.0.63 area 5
 network 172.18.205.128 0.0.0.63 area 5
 network 172.18.207.0 0.0.0.63 area 5
 network 172.18.207.64 0.0.0.31 area 5
 network 15.203.48.176 0.0.0.3 area 0
!
router rip
!
!
!
!
!
!
access-list 2 permit host 172.18.207.194
access-list 3 permit host 172.18.207.194
access-list 4 permit host 172.18.207.194
access-list 5 permit host 172.18.207.194
access-list 1 permit host 172.18.207.194
!
!
!
!
!
dial-peer voice 1 voip
 destination-pattern 1...
 session target ipv4:172.18.205.65
!
dial-peer voice 2 voip
 destination-pattern 2...
 session target ipv4:172.18.207.129
!
dial-peer voice 3 voip
 destination-pattern 3...
 session target ipv4:172.18.206.193
!
dial-peer voice 4 voip
 destination-pattern 4...
 session target ipv4:172.18.207.161
!
telephony-service
 no auto-reg-ephone
 max-ephones 20
 max-dn 20
 ip source-address 172.18.207.1 port 2000
!
ephone-dn 1
 number 5001
!
ephone-dn 2
 number 5002
!
ephone 1
 device-security-mode none
 mac-address 0001.6349.3AC0
 type 7960
 button 1:1
!
ephone 2
 device-security-mode none
 mac-address 00D0.97DC.0818
 type 7960
 button 1:2
!
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end

