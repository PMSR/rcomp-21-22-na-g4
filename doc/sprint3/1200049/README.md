RCOMP 2021-2022 Project - Sprint 3 - Member 1200049 folder
===========================================
(This folder is to be created/edited by the team member 1200049 only)

#### This is just an example for a team member with number 1200049 ####
### Each member should create a folder similar to this, matching his/her number. ###
The owner of this folder (member number 2222222) will commit here all the outcomes (results/artifacts/products)		       of his/her work during sprint 3. This may encompass any kind of standard file types.

* All commands used for the new specifications can be retrieved on the configfiles folder.

### HTTP page address:
Beyond the regular existent DMZ server that is now a DNS server, we also have a http server which provides a web page on the address 172.18.207.67

## OSPF dynamic routing

| Building       | Area id |
| -------------- | ------- |
| Backbone       | 0       |
| Building 1     | 1       |
| Building 2     | 2       |
| Building 3     | 3       |
| Building 4 | 4   |
| Building 5     | 5       |

* Static routes were deleted from the routing table, and the OSPF was established at Building 5 router.

## DHCPv4 service

The router on building 4 provide the DHCPv4 service to all local networks except for DMZ networks, were IPv4 node address are static and manually set (routers and servers).

## VoIP service

Ports of switches connecting to Cisco IP phones 7960, must have the voice VLAN enabled (switchport voice vlan VLANID) and the access VLAN disabled (no switchport access vlan).
Regarding VoIP call forwarding, when an incoming call is for a phone prefix assigned to another building, then the call must be forward to the telephony services running in that building’s router.

| Building | Prefix digits   | Floor | Phones Numbers   |
| -------- | --------------- | ----- | ---------------- |
| 5        | 5... | 0     | 5001 |
| 5        |      | 1     | 5002       |

## DNS

| DNS location                  | Name                            |
| ----------------------------- | ------------------------------- |
| Backbone - DNS root domain    | rcomp-21-22-na-g4               |
| Backbone - DNS server name    | ns.rcomp-21-22-na-g4            |
| Building 5 - Local DNS domain | building-5.rcomp-21-22-na-g4    |
| Building 5 - DNS server name  | ns.building-5.rcomp-21-22-na-g4 |

IPv4 node address of the DNS name server:
> Building 4: 172.18.207.67/26 (Mask: 255.255.255.192)