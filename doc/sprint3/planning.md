RCOMP 2021-2022 Project - Sprint 3 planning
===========================================
### Sprint master: 1140985 ###
(This file is to be created/edited by the sprint master only)
# 1. Sprint's backlog #
In this sprint, each team member will keep working on the same network simulation from the previous
sprint, Sprint 2 (concerning the same building). From the already established layer three configurations, now,
OSPF based dynamic routing will be used to replace the previously applied static routing.
Beyond OSPF based dynamic routing, other configurations included in this sprint are:
* DHCPv4 service
* VoIP service
* Adding a second server to each DMZ network to run the HTTP service.
* Configuring DNS servers to establish a DNS domains tree.
* Enforcing NAT (Network Address Translation).
* Establishing traffic access policies on routers (static firewall).


# 2. Technical decisions and coordination #
In this section, all technical decisions taken in the planning meeting should be mentioned. 		Most importantly, all technical decisions impacting on the subtasks implementation must be settled on this 		meeting and specified here.
* configure OSPF as dynamic routing protocol
   * define area id to each *building* (**must be different**)
   * define area id to *backbone*
* define DNS server IPv4 addresses
* define DNS root domain and building's sub-domain

#### Examples: ####
  * Backbone cable types to be used
  * VLAN IDs to be used
  * VTP domains
  * WiFi channels
  * IPv4 networks' addresses and routers' addresses
  * Routing protocols
  * Application protocols outlining (further coordination may be required between members)

# 3. Subtasks assignment #
(For each team member (sprint master included), the description of the assigned subtask in sprint 3)

**1140985** 
> Update the campus.pkt layer three Packet Tracer simulation from the
    previous sprint, to include the described features in this sprint for
    **building 2**.

> Final integration of each member’s Packet Tracer simulation into a
    single simulation.


**1160810**
> Update the campus.pkt layer three Packet Tracer simulation from the
      previous sprint, to include the described features in this sprint for
      **building 1**.


**1180651**
>   Update the campus.pkt layer three Packet Tracer simulation from the
    previous sprint, to include the described features in this sprint for
    **building 3**.


**1191505**
  
>   Update the campus.pkt layer three Packet Tracer simulation from the
        previous sprint, to include the described features in this sprint for
        **building 4**.


**1200049**
> Update the campus.pkt layer three Packet Tracer simulation from the
      previous sprint, to include the described features in this sprint for
      **building 5**.

# OSPF areas #

* backbone -> area 0
* building 1 -> area 1
* building 2 -> area 2
* building 3 -> area 3
* building 4 -> area 4
* building 5 -> area 5


# DNS #
## domain and sub-domain specifications ##

* domain: rcomp-21-22-na-g4
* sub-domain: building-X.rcomp-21-22-na-g4

| Building   	| Sub-domain                   	|
|------------	|------------------------------	|
| Building 1 	| building-1.rcomp-21-22-na-g4 	|
| Building 2 	| building-2.rcomp-21-22-na-g4 	|
| Building 3 	| building-3.rcomp-21-22-na-g4 	|
| Building 4 	| building-4.rcomp-21-22-na-g4 	|
| Building 5 	| building-5.rcomp-21-22-na-g4 	|


