# US5001 - As Project Manager, I want that the team start developing the input communication module of the AGV digital twin to accept requests from the "AGVManager"  
# &
# US5002 - As Project Manager, I want that the team start developing the output communication module of the AGV digital twin to update its status on the "AGVManager"
===================================================
(This folder is to be created/edited by the team member 1191505 only)

[//]: # (&#40;5001 - Como Gerente de Projeto, quero que a equipe comece a desenvolver o módulo de comunicação de entrada do gêmeo digital AGV para aceitar solicitações do "AGVManager"&#41;)

[//]: # (&#40;5002 - Como Gerente de Projeto, quero que a equipe comece a desenvolver o módulo de comunicação de saída do gêmeo digital AGV para atualizar seu status no "AGVManager"&#41;)

## Sprint Backlog
In this US's it's necessary to do data transactions applications using TCP protocol, meaning a Protocol SPOMS.
The SPOMS Protocol aims to facilitate data exchange between the applications comprising the system being developed and in line with previous recommendation.

**SPOMS role:**
** **AVG (Digital Twin)** - Server application and client application.

**Acceptance Criteria/Comments for US5001:**
* It must be used the provided application protocol (SPOMS2022).
* It is suggested the adoptiong of concurrent mechanisms (e.g. threads) and state sharing between these mechanisms.
* In this sprint, for demonstration purposes, it is acceptable to mock processing some of the incoming requests to foster some output communication.

**Acceptance Criteria/Comments for US5002:**
* It must be used the provided application protocol (SPOMS2022).
* It is suggested the adoptiong of concurrent mechanisms (e.g. threads) and state sharing between these mechanisms.
* In this sprint, for demonstration purposes, it is acceptable to mock processing some of the incoming requests to foster some output communication.

## Technical decisions

#### SOMSP Message Formate  
````
* Version = 1;
* Code = [0-255];
* D_Lenght_1 = [0-255]
* D_Lenght_2 = [0-255]
* Data
````
**Note:** D_Lenght_1 + 256 * D_Lenght_2.

#### SOMSP Message Codes
````
0 - COMMTEST (Communications test request with no other effect on the server application than the response with a code two message (ACK). This request has no data).
1 - DISCONN (End of session request. The server is supposed to respond with a code two message, afterwards both applications are expected to close the session (TCP connection). This request has no data.).
2 - ACK (Generic acknowledgment message. Used in response to requests with codes zero and one but may be used for other requests. This response has no data).
````

## Java Files

![Classes](Classes.jpg)

The Java Files can be found on Project/api.agvmanager/src/main/java/SPOMS.

## Result

![Result_Com](Result_Com.jpg)

## Considerations

Since I don't have the LAPR4 course and therefore I haven't had the project since the beginning of the semester, I had a lot of difficulties trying to understand what was being done and in turn implementing what was requested.
