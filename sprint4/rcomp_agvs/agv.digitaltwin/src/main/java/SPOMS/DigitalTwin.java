package SPOMS;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Random;
import java.util.Scanner;

public class DigitalTwin {

    static final String IP_ADDRESS="0.0.0.0";
    static final int PORT=9999;

    public static void main(String args[]) throws IOException {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Please, enter de clients certificate name: ");
            String certifaceteName = scanner.next();

            TwinCli twinCli = new TwinCli(IP_ADDRESS, PORT, new Random().nextInt(999999), certifaceteName);
            twinCli.start();

    }
}
