package SPOMS;

import spoms.SPOMSP;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.*;
import java.net.Socket;
import java.sql.Timestamp;
import java.util.Random;

public class TwinCli extends Thread implements SPOMSP {

    SSLSocket sock;
    static final String KEYSTORE_PASS="agvmanager";

    private int id;

    public TwinCli(String ip, int port, int id, String certName){
        this.id = id;
        System.setProperty("javax.net.ssl.trustStore", "static/"+ certName +".jks");
        System.setProperty("javax.net.ssl.trustStorePassword",KEYSTORE_PASS);

        System.setProperty("javax.net.ssl.keyStore","static/"+ certName +".jks");
        System.setProperty("javax.net.ssl.keyStorePassword",KEYSTORE_PASS);

        SSLSocketFactory sf = (SSLSocketFactory) SSLSocketFactory.getDefault();
        try {
            sock = (SSLSocket) sf.createSocket(ip, port);
            sock.startHandshake();
            this.sendMessage(Code.COMMTEST);
            if(!this.receivesAnswer(Code.ACK)){
                throw new IOException();
            }
            //lauching Dashboard Positions
            this.sendMessage(Code.DASHBOARD);
        }
        catch(IOException ex) {
            System.out.println("Failed to connect.");
            System.exit(1); }
    }

    public void start(){
        try {
            String textTest;
            int k=0;
            byte [] data;
            Random rand = new Random();
            while(true) {
                if(k==15){
                    this.sendMessage(Code.DISCONN);
                    break;
                }
                textTest=String.format("agvId:%d/ posX:%d;posY:%d/ timestamp:%s%n",
                        id,
                        rand.nextInt(25),
                        rand.nextInt(25),
                        new Timestamp(System.currentTimeMillis()
                        ));
                System.out.println(textTest);
                data=textTest.getBytes();
                this.sendMessage(SPOMSP.Code.POSITION, data);
                k++;
                sleep((rand.nextInt(10000))+1000);
            }

            System.out.println("Client  disconnected");
            sock.close();
        }
        catch(IOException ex) { System.out.println("IOException"); } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Socket getSocket() {
        return this.sock;
    }
}
