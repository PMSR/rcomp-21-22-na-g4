package RecievePositionsServer;

import spoms.SPOMSP;

import HTTP.HttpsServerAjaxVoting;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

public class AgvManagerController extends Thread implements SPOMSP {
        private Socket s;
        private DataOutputStream sOut;
        private DataInputStream sIn;

        public AgvManagerController(Socket cli_s) { s=cli_s;}

        public void run() {
            InetAddress clientIP;

            clientIP=s.getInetAddress();
            System.out.println("New client connection from " + clientIP.getHostAddress() +
                    ", port number " + s.getPort());

            System.out.println("Assign a task to the agv");
            try {
                sOut = new DataOutputStream(s.getOutputStream());
                sIn = new DataInputStream(s.getInputStream());
                loop: while(true) {
                    byte version = sIn.readByte();
                    System.out.println("Version: " + version + "\n");

                    byte code = sIn.readByte();
                    Code code_i = Code.values()[code];
                    System.out.println("Code: " + code_i + "\n");

                    byte d_length_1 = sIn.readByte();
                    System.out.println("d_length_1: " + d_length_1 + "\n");

                    byte d_length_2 = sIn.readByte();
                    System.out.println("d_length_2: " + d_length_2 + "\n");

                    int finalLength = d_length_1 + (256 * d_length_2);

                    switch (code_i) {
                        case COMMTEST:
                            sendMessage(Code.ACK);
                            break;

                        case DASHBOARD:
                            System.out.println("Returning AGV Positions to Dashboard: ");
                            //sendPositionsToDashboard();
                            //HttpAjaxVotingRequest dashboard = new HttpAjaxVotingRequest();
                            //launchDashboard();
                            break;

                        case POSITION:
                            byte[] buffer = new byte[finalLength];
                            sIn.read(buffer, 0, finalLength);
                            System.out.println("this "+ new String(buffer, 0, finalLength));
                            TcpSrv.writeToFile(new String(buffer, 0, finalLength));
                            sendMessage(Code.ACK);
                            break;

                        case DISCONN:
                            System.out.println("End Session!");
                            break loop;

                    }
                }

                System.out.println("Client " + clientIP.getHostAddress() + ", port number: " + s.getPort() +
                        " disconnected");
                s.close();
            }
            catch(IOException ex) { ex.printStackTrace(); }
        }

    @Override
    public Socket getSocket() {
        return s;
    }
}
