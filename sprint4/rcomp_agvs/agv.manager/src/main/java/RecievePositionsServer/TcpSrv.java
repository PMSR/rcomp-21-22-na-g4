package RecievePositionsServer;

import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.LinkedList;

class TcpSrv {
    static SSLServerSocket sock;
    private static File positionsFile;
    private static LinkedList<String> positions;
    static final String KEYSTORE_PASS="agvmanager";
    static final String TRUSTED_STORE="static/server_agv_manager.jks";

    public static synchronized void writeToFile(String data){
        try (FileWriter fw = new FileWriter(positionsFile.getName(), false)){
            if(positions.size()>=15){
                positions.removeLast();
            }
            positions.addFirst(data);
            fw.write(positions.toString());
        } catch (IOException e) {
            System.out.println("error positionsfile");
            e.printStackTrace();
        }
    }


    public static void main(String args[]) throws Exception {
        positionsFile = new File("positions.txt");
        positions = new LinkedList<>();

        System.setProperty("javax.net.ssl.trustStore", TRUSTED_STORE);
        System.setProperty("javax.net.ssl.trustStorePassword",KEYSTORE_PASS);

        // Use this certificate and private key as server certificate
        System.setProperty("javax.net.ssl.keyStore",TRUSTED_STORE);
        System.setProperty("javax.net.ssl.keyStorePassword",KEYSTORE_PASS);

        SSLServerSocketFactory sslF = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();

        try {
            sock = (SSLServerSocket) sslF.createServerSocket(9999);
            sock.setNeedClientAuth(true);
        }
        catch(IOException ex) {
            System.out.println("Failed to open server socket");
            System.exit(1);
        }

        while(true) {
            System.out.println(sock.getLocalSocketAddress());
            Socket cliSock = sock.accept();
            Thread cli = new AgvManagerController(cliSock);
            cli.start();
        }
    }
}


