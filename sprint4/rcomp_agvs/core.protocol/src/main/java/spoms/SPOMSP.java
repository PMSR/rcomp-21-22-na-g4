package spoms;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public interface SPOMSP {

    Socket getSocket();

    int version = 1, max_bytes = 255;

    default void sendMessage(Code code) throws IOException {
        DataOutputStream dOut = new DataOutputStream(getSocket().getOutputStream());
        byte[] message = {(byte) 0, (byte) code.code, 0, 0};
        dOut.write(message);
    }

    default void sendMessage(Code code, byte[] data) throws IOException {
        int dataLength = data.length;
        byte D_LENGTH_1 = (byte)(dataLength % 255);
        byte D_LENGTH_2 = (byte)(dataLength / 255);
        DataOutputStream dOut = new DataOutputStream(getSocket().getOutputStream());
        byte[] message = {(byte) 0, (byte) code.code, D_LENGTH_1, D_LENGTH_2};
        dOut.write(message);
        dOut.write(data);
    }




    default boolean receivesAnswer(Code answer) throws IOException {
        DataInputStream dIn = new DataInputStream(getSocket().getInputStream());
        dIn.readByte();
        Code code = Code.valueOf(dIn.readByte());
        int n_bytes = dIn.readByte();
        if (n_bytes != 0) {
            for (int i = 0; i < n_bytes; i++) {
                dIn.readByte();
            }
        }
        if (code != answer) {
            System.out.println("Received message " + code + ".\n");
            return false;
        }
        return true;
    }

    default void closeConnection() throws IOException {
        System.out.println("Close Connection!\n");
        sendMessage(Code.DISCONN);
        getSocket().close();
        if (!receivesAnswer(Code.ACK)) {
            throw new IOException("Can't close the connection from the server!\n");
        }
    }

    enum Code {
        COMMTEST(0),
        DISCONN(1),
        ACK(2),
        FILE_END(3),
        FILE_NAME(4),
        ERROR(5),
        AVAILABLE(6),
        POSITION(7),
        DASHBOARD(8);

        public int code;

        Code(int code) {
            this.code = code;
        }

        public static Code valueOf(int i) {
            for (Code code : Code.values()) {
                if (i == code.code) {
                    return code;
                }
            }
            return null;
        }
    }
}

/* Version - 1, Code - [0-255], D_lenght_1 - [0-255], D_lenght_2 - [0-255] */