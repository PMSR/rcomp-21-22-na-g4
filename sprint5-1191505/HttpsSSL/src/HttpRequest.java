import javax.net.ssl.SSLSocket;
import java.io.*;

class HttpRequest implements Runnable {
    private SSLSocket s;
    private DataOutputStream sOut;
    private DataInputStream sIn;
    private String baseFolder;

    public HttpRequest(SSLSocket cli_s, String folder) {
        s = cli_s;
        baseFolder = folder;
    }

    public void run() {
        try {
            sOut = new DataOutputStream(s.getOutputStream());
            sIn = new DataInputStream(s.getInputStream());
        } catch (IOException ex) {
            System.out.println("Data Stream IOException");
        }
        String request = HTTPS.readLineCRLF(sIn);
        if (request.startsWith("POST /commtest")) {
            processCommtest();
        } else if (request.startsWith("POST /disconn")) {
            processDisconn();
            try {
                s.close();
            } catch (IOException e) {
                System.out.println("CLOSE IOException");
                ;
            }
        } else if (request.startsWith("POST /upload")) {
            processPostUpload();
        } else if (request.startsWith("POST /list")) {
            processPostList();
        } else processGet(request);

        try {
            s.close();
        } catch (IOException ex) {
            System.out.println("CLOSE IOException");
        }
    }

    void processGet(String req) {
        String line, fileName, filePath;

        do { // READ AND IGNORE THE HEADER LINES
            line = HTTPS.readLineCRLF(sIn);
        }
        while (line.length() > 0);

        fileName = req.split(" ")[1];
        if (fileName.compareTo("/") == 0) fileName = "/index.html";
        filePath = baseFolder + fileName;
        HTTPS.sendHttpFileResponse(sOut, null, filePath);
    }

    void processCommtest() {
        HTTPS.sendHttpStringResponse(sOut, "200 Ok", "text/html", "<html><head><title>Commtest</title></head><body><h1>The server is running...<br />Please select one of the options upload file and download file from the list</h1><big><ul>");
    }

    void processDisconn() {
        closeConnection();
        HTTPS.sendHttpStringResponse(sOut, "200 Ok", "text/html",
                "<html><head><title>SDisconn</title></head><body><center></center>"
                        + "<h1><h1>Please you have to select one of the options upload file and download file from the list the connection is close<br />" +
                        "<hr><p><a href=/>BACK</a></body></html>");
    }

    void closeConnection() {
        try {
            s.close();
        } catch (IOException ex) {
            System.out.println("CLOSE IOException");
        }

    }

    void processPostUpload() {
        String line, boundary, filename, filePath;
        int done, readNow, len;
        String cDisp = "Content-Disposition: form-data; name=\"filename\"; filename=\"";
        File f;
        FileOutputStream fOut;
        byte[] data = new byte[65792];

        len = 0;
        boundary = null;
        do {    // FIRSH HEADER
            line = HTTPS.readLineCRLF(sIn);
            if (line.startsWith("Content-Length: ")) {
                len = Integer.parseInt(line.split(" ")[1]);
            } else if (line.startsWith("Content-Type: multipart/form-data; boundary=")) {
                boundary = line.split("=")[1];
            }
        }
        while (line.length() > 0);

        if (len == 0) {
            replyPostError("Content-Length: expected and not found");
            return;
        }
        if (boundary == null) {
            replyPostError("Content-Type: multipart/form-data; expected and not found");
            return;
        }

        line = HTTPS.readLineCRLF(sIn);
        if (!line.endsWith(boundary)) {
            replyPostError("Multipart separator expected and not found");
            return;
        }
        len = len - line.length() - 2;
        filename = "";
        do {    // SECOND HEADER
            line = HTTPS.readLineCRLF(sIn);
            len = len - line.length() - 2;
            if (line.startsWith(cDisp)) {
                filename = line.split("=")[2];
                filename = filename.substring(1, filename.length() - 1);
            }
        }
        while (line.length() > 0);

        try {
            if (filename.length() == 0) {
                do {
                    done = sIn.read(data, 0, 65792);
                    len = len - done;
                } while (len > 0);
                replyPostError("Content-Disposition: form-data; expected and not found (NO FILENAME)");
                return;
            }
            filePath = baseFolder + "/" + filename;
            f = new File(filePath);
            fOut = new FileOutputStream(f);

            len = len - boundary.length() - 6;

            do {
                if (len > 65792) readNow = 65792;
                else readNow = len;
                done = sIn.read(data, 0, readNow);
                fOut.write(data, 0, done);
                len = len - done;
            }
            while (len > 0);
            fOut.close();
            line = HTTPS.readLineCRLF(sIn);
        } catch (IOException ex) {
            System.out.println("IOException");
        }
        replyPostList();
    }


    void processPostList() {
        String line;
        do { // READ AND IGNORE THE HEADER LINES
            line = HTTPS.readLineCRLF(sIn);
        }
        while (line.length() > 0);
        replyPostList();
    }


    void replyPostList() {
        String s1 = "<html><head><title>File List</title></head><body><h1>File List:</h1><big><ul>";
        String s2 = "</ul></big><hr><p><a href=/>BACK</a></body></html>";
        String list;
        File d;

        d = new File("www/");
        list = s1;
        File[] filesList = d.listFiles();
        for (File f : filesList)
            if (f.isFile())
                list = list + "<li><a href=/" + f.getName() + ">" + f.getName() + "</a>";
        list = list + s2;
        HTTPS.sendHttpStringResponse(sOut, "200 Ok", "text/html", list);
    }


    void replyPostError(String error) {
        HTTPS.sendHttpStringResponse(sOut, "500 Internal Server Error", "text/html",
                "<html><head><title>Server Error</title></head><body><center><img src=500.png><br></center>"
                        + "<h1>Server error on POST</h1><p>ERROR: " + error +
                        "<hr><p><a href=/>BACK</a></body></html>");
    }
}
