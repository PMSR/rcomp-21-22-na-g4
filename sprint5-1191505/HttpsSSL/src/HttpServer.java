import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import java.io.IOException;
import java.net.InetAddress;

class HttpServer {
    static private final String BASE_FOLDER = "www";
    static private SSLServerSocket sock;
    static InetAddress clientIP;
//    static final int SERVER_PORT = 8758;

    public static void main(String args[]) throws Exception {
        SSLSocket cliSock;

        if (args.length != 1) {
            System.out.println("Local port number required at the command line.");
            System.exit(1);
        }

//        // Trust these certificates provided by authorized clients
//        System.setProperty("javax.net.ssl.trustStore", "HttpServer.jks");
//        System.setProperty("javax.net.ssl.trustStorePassword", "forgotten");

        // Use this certificate and private key as server certificate
        System.setProperty("javax.net.ssl.keyStore", "HttpServer.jks");
        System.setProperty("javax.net.ssl.keyStorePassword", "forgotten");

        try {
            SSLServerSocketFactory sslF = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
            sock = (SSLServerSocket) sslF.createServerSocket(Integer.parseInt(args[0]));
            System.out.println("The Distribution Centre is running on port " + sock.getLocalPort());
        } catch (IOException ex) {
            System.out.println("Failed to open local port " + args[0]);
            System.out.println("Failed to open server socket!");
            System.exit(1);
        }
        while (true) {
            cliSock = (SSLSocket) sock.accept();
            clientIP = sock.getInetAddress();
            System.out.println("New client connection from " + clientIP.getHostAddress() + ", port number " + cliSock.getPort());
            new Thread(new HttpRequest(cliSock, BASE_FOLDER)).start();
        }
    }
}
