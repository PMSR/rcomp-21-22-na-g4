import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Client {

    static InetAddress serverIP;
    static SSLSocket sock;
    static final int SERVER_PORT = 8788;
    static final String KEYSTORE_PASS = "forgotten";

    public static void main(String args[]) throws Exception {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please, insert the IP Address from the server: ");
        String ipServer = scanner.next();

        // Trust these certificates provided by servers
        System.setProperty("javax.net.ssl.trustStore", "Client.jks");
        System.setProperty("javax.net.ssl.trustStorePassword", KEYSTORE_PASS);

        // Use this certificate and private key for client certificate when requested by the server
        System.setProperty("javax.net.ssl.keyStore","Client.jks");
        System.setProperty("javax.net.ssl.keyStorePassword", KEYSTORE_PASS);

        SSLSocketFactory sf = (SSLSocketFactory) SSLSocketFactory.getDefault();


        try {
            serverIP = InetAddress.getByName(ipServer);
        } catch (UnknownHostException ex) {
            System.out.println("Invalid server specified: " + ipServer);
            System.exit(1);
        }

        try {
            sock = (SSLSocket) sf.createSocket(ipServer, SERVER_PORT);
            System.out.println("Connection accept on port " + sock.getPort());
        } catch (IOException ex) {
            System.out.println("Failed to establish TCP connection");
            System.out.println("Application aborted.");
            System.exit(1);
        }

        sock.startHandshake();

        byte version = 1;
        System.out.println("Version " + version);

        String option;//user's determined option.
        //Will continue asking user to enter one of 4 commands until entered properly.
        System.out.println("\nWould you like to COMMTEST or DISCONN or SEND or RECEIVE");
        do {
            option = scanner.nextLine();
            //switch statement will handle the 4 users choices.
            switch (option) {
                case "COMMTEST":
                    String msg = "COMMTEST";
                    sock.getOutputStream().write(msg.getBytes());
                    System.out.println("Communication test request succefull.");
                    sock.close();
                    break;

                case "DISCONN":
                    msg = "DISCONN";
                    sock.getOutputStream().write(msg.getBytes());
//                    System.out.println(ack(1));
                    System.out.println("Start to end session...");
                    sock.close();
                    System.exit(0);
                    break;

                //Simulates a download of a file from the server
                case "RECEIVE":

                    //folder holds one file at a time in FilesHost directory
                    File folder = new File("/users/home/1191505/rcomp-21-22-na-1191505/SSLTCP/FilesHost/");
                    //array reads all files in directory and stores them
                    File[] listOfFiles = folder.listFiles();
                    System.out.println("\nList of all the current server files");
                    //displays all the files in the directory to the user.
                    for (File listOfFile : listOfFiles) {
                        if (listOfFile.isFile()) {
                            System.out.println("File: " + listOfFile.getName());
                        }
                    }
                    //prompt user to choose from FilesHost
                    System.out.println("\nWhich file would you like to receive? (filename and extention)");
                    String getFile = scanner.nextLine().toString();

                    msg = "RECEIVE";//send to server to invoke it's send method
                    String name = getFile;

                    sock.getOutputStream().write(msg.getBytes());
                    //Send the file name to the Server so it can retrieve the right one
                    sock.getOutputStream().write(name.getBytes());

                    //array lenth determines how many bytes we send at a time
                    byte[] myByteArray = new byte[1];
                    //get input stream from Server.
                    InputStream is = sock.getInputStream();
                    //Outputs the retrieved file to the Client Files directory under the specified name
                    FileOutputStream fos = new FileOutputStream("/users/home/1191505/rcomp-21-22-na-1191505/SSLTCP/Files/" + getFile.trim());//trim off array space not used.
                    BufferedOutputStream bos = new BufferedOutputStream(fos);
                    //used to deplete the array. Continue reading until myByteArray is empty
                    int bytesRead = is.read(myByteArray, 0, myByteArray.length);
                    int len = 0;
                    //write the bytes until reached file length
                    while ((len = is.read(myByteArray)) != -1) {
                        bos.write(myByteArray);
                    }
                    //notify user
                    System.out.println(getFile + " has been successfully received to the client files directory.");
                    bos.close();
                    sock.close();
                    break;


                //simulates uploading a file to the server
                case "SEND":
                    msg = "SEND";//initial message
                    //send message to Server to invoke method
                    sock.getOutputStream().write(msg.getBytes());
                    //prompt user to enter path of file to upload
                    System.out.println("Please enter the path of the file to send: "
                            + "\n EX:./users/home/1191505/rcomp-21-22-na-1191505/SSLTCP/Files/Dados.txt\n");
                    //store path of user's file
                    String userFile = scanner.nextLine();
                    name = userFile;

                    //If statement to extract the file and extension from the full path
                    //If the file path is used with back slashes
                    if (name.contains("\\")) {
                        name = name.replaceAll(".*\\\\", "");
                    } else {
                        //If the file path is used with forward slashes
                        name = name.replaceAll(".*/", "");
                    }
                    //Send the file name to the Server so it can store it as such
                    sock.getOutputStream().write(name.getBytes());
                    //store as type File, the determined path of file to transfer
                    File myFile = new File(userFile);
                    //array is set to length of file.
                    byte[] mybytearray = new byte[(int) myFile.length()];
                    OutputStream os = sock.getOutputStream();
                    //used to write bytes to underlying output stream
                    BufferedInputStream bis = new BufferedInputStream(new FileInputStream(myFile));
                    bis.read(mybytearray, 0, mybytearray.length);
                    os.write(mybytearray, 0, mybytearray.length);
                    //Notify user.
                    System.out.println("File " + name + " has been sent.");
                    //Flush and close socket. Close program.
                    os.flush();
                    sock.close();
                    System.exit(0);

                    break;
            }
        } while (true);
    }

    public static String ack(int code) {
        String msg = null;
        if (code == 0) {
            msg = "Communication test request succefull.";
        } else if (code == 1) {
            msg = "Start to end session...";
        }
        return msg;
    }
}