import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import java.io.IOException;
import java.net.InetAddress;

class DistributionCentre {
    static InetAddress clientIP;
    static final int SERVER_PORT = 8788;
    static String fileName;

    static final String TRUSTED_STORE = "DistributionCentre.jks";
    static final String KEYSTORE_PASS = "forgotten";

    public static void main(String args[]) throws Exception {
        SSLServerSocket sf = null;
//        Socket sock;
        SSLSocket sock;

        // Trust these certificates provided by authorized clients
        System.setProperty("javax.net.ssl.trustStore", TRUSTED_STORE);
        System.setProperty("javax.net.ssl.trustStorePassword",KEYSTORE_PASS);

        // Use this certificate and private key as server certificate
        System.setProperty("javax.net.ssl.keyStore",TRUSTED_STORE);
        System.setProperty("javax.net.ssl.keyStorePassword",KEYSTORE_PASS);

        SSLServerSocketFactory sslF = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();

        try {
            sf = (SSLServerSocket) sslF.createServerSocket(SERVER_PORT);
            sf.setNeedClientAuth(true);
            System.out.println("The Distribution Centre is running on port " + sf.getLocalPort());
        } catch (IOException ex) {
            System.out.println("Server failed to open local port " + SERVER_PORT);
            System.exit(1);
        }

        while (true) {
            sock = (SSLSocket) sf.accept();
            clientIP = sock.getInetAddress();
            System.out.println("New client connection from " + clientIP.getHostAddress() + ", port number " + sock.getPort());

            //Used to hold the initial client message
            byte[] msgArray = new byte[7];
            //Allows for a file name to be up to 50 characters (including extension)
            byte[] nameArray = new byte[50];

            //Accept Client message and store it
            sock.getInputStream().read(msgArray);
            String readableMsg = new String(msgArray, "UTF-8");
            //Trim excess space in array
            readableMsg = readableMsg.trim();


            sock.getInputStream().read(nameArray);
            //Converts the input stream to an array using the UTF-8 format
            fileName = new String(nameArray, "UTF-8");
            //Removes the extra spaces not being used of the set 50 bytes in the previously stated array
            fileName = fileName.trim();

            //Takes User Message and determines which method to invoke
            System.out.println(readableMsg);
            if (readableMsg.equals("COMMTEST")) {
                Client.ack(0);
                break;
            } else if (readableMsg.equals("DISCONN")) {
                Client.ack(1);
                FilesHost.closeConnection(sock);
                break;
            } else if (readableMsg.equals("RECEIVE")) {
                FilesHost.SendToClient(sock);
            } else if (readableMsg.equals("SEND")) {
                FilesHost.ReceiveFromClient(sock);//
            }
        }
    }
}