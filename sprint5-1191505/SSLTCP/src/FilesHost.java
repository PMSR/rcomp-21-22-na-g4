import javax.net.ssl.SSLSocket;
import java.io.*;
public class FilesHost {
    public static void SendToClient(SSLSocket sock) throws IOException {


        //Pulls the specified file from the ServerFiles directory based on the specified file name
        File myFile = new File("/users/home/1191505/rcomp-21-22-na-1191505/SSLTCP/FilesHost/" + DistributionCentre.fileName);
        //Array lenghth is file size. Client will keep accepting bytes until the array is empty
        byte[] mybytearray = new byte[65792];
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(myFile));
        bis.read(mybytearray, 0, mybytearray.length);

        //Get output stream from client
        OutputStream os = sock.getOutputStream();

        //Writing file to the Output Stream
        os.write(mybytearray, 0, mybytearray.length);

        //Inform user
        System.out.println("File Sent");

        //Flush then close the socket
        os.flush();
        sock.close();
        System.exit(0);
    }

    public static void ReceiveFromClient(SSLSocket sock) throws IOException {

        //Length 1, the amount of bytes will be sending over at a time
        byte[] myByteArray = new byte[1];
        InputStream is = sock.getInputStream();

        //The output directory is set to be the ServerFiles directory
        //The output file is set to be the exact same name as the input (name and extension)
        FileOutputStream fos = new FileOutputStream("/users/home/1191505/rcomp-21-22-na-1191505/SSLTCP/FilesHost/" + DistributionCentre.fileName);

        //used to write bytes to underlying output stream
        BufferedOutputStream bos = new BufferedOutputStream(fos);

        //used to depleate array. Server keeps reading bytes until it reaches 0
        int bytesRead = is.read(myByteArray, 0, myByteArray.length);
        int len=0;

        //write the bytes until reached file length
        while((len = is.read(myByteArray))!= -1){
            bos.write(myByteArray);
        }
        //notify the user. Close the socket, close program
        System.out.println("File Received");
        bos.close();
        sock.close();
        System.exit(0);
    }
    public static void closeConnection(SSLSocket sock) throws IOException {
        sock.close();
    }
}

//data = 256 + 256 * 256 = 65792
