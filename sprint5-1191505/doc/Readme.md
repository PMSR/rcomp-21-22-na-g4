[//]: # (By default, servers don’t demand a client’s certificate.)
RCOMP 2021-2022 Project - Sprint 5 - Member 1191505 folder
===========================================
(This folder is to be created/edited by the team member 1191505 only)

# SPOMS Protocol

## Objective

The purpose of this application is facilitating data exchanges between applications, in other words, the system's is storing files in a distributed network applications environment. 

This application, is based in a **TCP** (Transmission Control Protocol) and uses the client-server model.

| Application | Objective                                                                                                                                                                     |
|-------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Client      | Takes the initiative off requesting a TCP connection establishment with the counterpart server application. Is allowed to take the initiative of sending data, **a request**. |
| Server      | Accepts the connection request from client and must be passively waiting for a request and only then is authorized to send data, **a response** to the received request.      |

## SPOMSP message format

| Field      | Offset (bytes) | Length (bytes) | Description                                                                                                                                        | 
|------------|----------------|----------------|----------------------------------------------------------------------------------------------------------------------------------------------------|
| VERSION    | 0              | 1              | Is a single byte and should be interpreted as an unsigned integer (0 to 255). Version = 1                                                          |
| CODE       | 1              | 1              | Identifies the type of request or response, it should be interpreted as an unsigned integer (0 to 255).                                            |
| D_LENGHT_1 | 2              | 1              | Specify the length in bytes of the DATA field. Interpreted as unsigned integer number (0 to 255). The length of DATA field is to be calculated as: |
| D_LENGHT_2 | 3              | 1              | **D_LENGHT_1 + 256 x D_LENGHT_2** The length may be 0 (does not exist).                                                                            |
| DATA       | 4              | -              | Contains data to meet the specific needs of the participating application.                                                                         |

## SPOMSP message codes 

| CODE | Meaning                                                                                                                                                               |
|------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 0    | **COMMTEST** - Communications test request with no other effect on the server application than the response with a code 2 (ACK). This request has **no data**.        | 
| 1    | **DISCONN** - End of session request. The server is supposed to respond with a code 2 (ACK), after both applications close the session. This request has **no data**. |
| 2    | **ACK** - Generic acknowledgment message. Used in response to requests with codes 0 and 1 but may be used for other requests. This response has **no data**.          |

**Note**: adding new message codes does not change the message format version.

## System to be implemented 

![Esquema.jpg](Esquema.jpg)

* **Classes for TCP:**
  * Distribution Centre (as server)
  * Clien (as client)
  * FilesHost

* **Classes for https:**
  * HTTP
  * HttpRequest
  * HttpServer

